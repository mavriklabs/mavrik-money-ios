//
//  PaymentDetalCell.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit

class PaymentDetailCell: UITableViewCell {
    
    static let cellID = "paymentDetalCell"
    static let closedHeight: CGFloat = 78
    static let openHeight: CGFloat = 180

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellCardInfo: UILabel!
    @IBOutlet weak var cellIndicator: UIImageView!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(_ data: LinkedPayment) {
        cellIndicator.transform = .identity
        cellTitle.text = data.name
        cellCardInfo.text = data.id
    }
    
    func buttonAnimation(vc: UIViewController) {
        deleteButton.alpha = 0
        Animations.animateView(type: .popIn, view: deleteButton, vc: vc, completion: {})
    }
    
    func rotateIndicator() {
        cellIndicator.transform = CGAffineTransform(rotationAngle: .pi)
    }

}
