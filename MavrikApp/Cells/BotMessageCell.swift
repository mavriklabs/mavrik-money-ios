//
//  BotMessageCell.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit

class BotMessageCell: UITableViewCell {
    
    static let cellID = "botMessageCell"
    static let height: CGFloat = 208
    
    var buttonDidTap: ((_ tag: Int)-> Void)?
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.withRadius(12)
        btn1.tag = 1
        btn1.addTarget(self, action: #selector(btnDidTap(_:)), for: .touchUpInside)
        btn2.tag = 2
        btn2.addTarget(self, action: #selector(btnDidTap(_:)), for: .touchUpInside)
        btn3.tag = 3
        btn3.addTarget(self, action: #selector(btnDidTap(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc private func btnDidTap(_ btn: UIButton) {
        buttonDidTap?(btn.tag)
    }

}
