//
//  CryptoBalanceCell.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit

class CryptoBalanceCell: UITableViewCell {
    
    static let cellID = "cryptoBalanceCell"
    static let height: CGFloat = 180

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellBalance: UILabel!
    @IBOutlet weak var sellDescr: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(_ data: CryptobalanceModel) {
        cellTitle.text = "\(data.name ?? "") (\(data.currency ?? ""))"
        cellBalance.text = "\(data.balance ?? 0)"
        sellDescr.text = getDescr(for: data)
    }
    
    private func getDescr(for data: CryptobalanceModel) -> String {
        let currency = "USD/\(data.currency ?? "")"
        let price = "$\(data.priceUSD ?? 0)"
        return currency + " " + price
    }

}
