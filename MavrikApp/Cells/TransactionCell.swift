//
//  TransactionCell.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    static let cellID = Constants.Cells.transactionCellId
    static let height: CGFloat = 104

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(_ model: AccountTransactions) {
        self.cellTitle.text = model.desc ?? ""
        self.cardLabel.text = (model.source ?? "") + " " + " \(model.dest ?? "")"
        self.amountLabel.text = getCurrencyPrice(model.amount ?? "0", currency: model.currency ?? "", type: model.type ?? "")
    }
    
    
    private func getCurrencyPrice(_ amount: String, currency: String, type: String) -> String {
        var newAmount = ""
        switch currency {
        case "USD":
            newAmount = "$\(amount)"
        default:
            newAmount = "\(amount) \(currency)"
        }
        return (type == "withdrawal") ? "-\(newAmount)" : newAmount
    }

}
