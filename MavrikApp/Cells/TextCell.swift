//
//  TextCell.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit

class TextCell: UITableViewCell {
    
    static let cellID = Constants.Cells.textCellId
    static let height: CGFloat = 240
    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var interestLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.withRadius(15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ data: AccountData) {
        cellTitle.text = data.name
        accountNumberLabel.text = data.id
        amountLabel.text = getCurrencyIcon(for: data.currency ?? "") + " " + "\(data.balance ?? 0)"
        interestLabel.text = "\(data.yield ?? "0%") Annually"
    }
    
    private func getCurrencyIcon(for str: String) -> String {
        switch str {
        case "USD":
            return "$"
        default:
            return ""
        }
    }
}
