//
//  LoginActivityCell.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit

class LoginActivityCell: UITableViewCell {
    
    static let cellID = "LoginActivityCell"
    static let height: CGFloat = 96
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var deviceLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(_ data: LoginActivity) {
        cityLabel.text = data.place
        deviceLabel.text = data.device
        dateLabel.text = getDate(data.lastActive ?? "")
    }
    
    private func getDate(_ str: String) -> String {
        let frmt = DateFormatter()
        frmt.dateFormat = "dd MMM yyyy HH:mma"
        if let date = frmt.date(from: str) {
            if Calendar.current.isDateInToday(date) {
                let newFrmt = DateFormatter()
                newFrmt.dateFormat = "HH:mm"
                return newFrmt.string(from: date)
            } else {
                let newFrmt = DateFormatter()
                newFrmt.dateFormat = "dd MMMM yyyy"
                return newFrmt.string(from: date)
            }
        }
        return ""
    }

}
