//
//  CountryCodeCell.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit

class CountryCodeCell: UITableViewCell {
    
    static let cellId = "CountryCodeCell"
    static let height: CGFloat = 60

    @IBOutlet weak var countryIcon: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ model: CountryCode) {
        countryIcon.image = UIImage(named: model.code)
        countryName.text = model.name
        countryCode.text = model.dial_code
    }

}
