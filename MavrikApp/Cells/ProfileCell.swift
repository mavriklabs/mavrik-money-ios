//
//  ProfileCell.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    static let cellID = "ProfileCell"
    static let height: CGFloat = 74

    @IBOutlet weak var cellIcon: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var cellDescr: UILabel!
    @IBOutlet weak var rightIndicator: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ model: ProfileCellModel) {
        cellIcon.image = UIImage(named: model.iconName)
        cellTitle.text = model.title
        if let descr = model.descr {
            cellDescr.isHidden = false
            cellDescr.text = descr
        } else {
            cellDescr.isHidden = true
        }
        if cellTitle.text == "Logout" {
            rightIndicator.isHidden = true
        }
    }
}
