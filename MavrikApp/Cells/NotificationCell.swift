//
//  NotificationCell.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    static let cellID = Constants.Notifications.cellId
    
    @IBOutlet weak var readIndicator: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var action: UILabel!
    @IBOutlet weak var timestamp: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCell(_ model: NotificationModel) {
        cellTitle.text = model.title
        descrLabel.text = model.desc
        readIndicator.isHidden = model.read ?? false
        timestamp.text = model.timestamp
        
        if model.action != nil && model.action != "" {
            action.text = model.action
            action.textColor = Constants.Colors.defaultOrange
            action.isHidden = false
        } else {
            action.isHidden = true
        }
    }
}
