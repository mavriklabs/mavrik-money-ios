//
//  YourMessageCell.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit

class YourMessageCell: UITableViewCell {
    
    static let cellID = "YourMessageCell"
    static let height: CGFloat = 52

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configCell(withText text: String) {
        messageText.text = text
        messageView.withRadius(8)
    }

}
