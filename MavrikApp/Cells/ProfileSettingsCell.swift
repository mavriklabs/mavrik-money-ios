//
//  ProfileSettingsCell.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit

class ProfileSettingsCell: UITableViewCell {
    
    static let cellID = "ProfileSettingsCell"
    static let height: CGFloat = 75
    
    @IBOutlet weak var cellTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
