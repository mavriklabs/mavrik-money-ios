//
//  Global.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/6/20.
//

import Foundation
    
func print(_ items: Any...) {
    #if DEBUG
        Swift.print(items)
    #endif
}

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
        var idx = items.startIndex
        let endIdx = items.endIndex
        
        repeat {
            Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
            idx += 1
        } while idx < endIdx
    #endif
}
