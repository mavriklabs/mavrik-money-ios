//
//  Constants.swift
//  MavrikApp
//
//  Created by AK on 20.10.2020.
//

import Foundation
import UIKit

struct Constants {
    
    struct URL {
        static let base = "mavrik.money"
        static let serverBase = "https://server." + base
        static let baseWithHttps = "https://" + base
        static let fDLDomain = "fdl." + base
        static let emailActionUrl = baseWithHttps + "__/auth/action"
        static let emailActionsPath = "/__/auth/action"
    }
    
    struct Colors {
        static let defaultBlue = UIColor(hex: "#6E8BF6")
        static let defaultOrange = UIColor(hex: "#F2A45A")
        static let defaultRed = UIColor(hex: "#ff6969ff")
        static let yellow = UIColor(red: 246/256, green: 217/256, blue: 110/256, alpha: 1)
        static let lightGray = "#ededed"
        static let cryptoCellShadowColor = UIColor(red: 0.949, green: 0.643, blue: 0.353, alpha: 0.2)
        static let stockCellShadowColor = UIColor(red: 0.236, green: 0.765, blue: 0.992, alpha: 0.2)
        static let artCellShadowColor = UIColor(red: 1, green: 0.256, blue: 0.154, alpha: 0.2)
        static let realEstateCellShadowColor = UIColor(red: 0.982, green: 1, blue: 0.108, alpha: 0.2)
        static let txnCellShadowColor = UIColor(red: 0.659, green: 0.236, blue: 0.992, alpha: 0.1)
    }
    
    struct Images {
        static let gradButton = "gradButton"
        static let profileIcon = "profileIcon"
        static let faceIcon = "faceIcon"
        static let touchIcon = "touchIcon"
    }
    
    struct LottieAnimatedIcons {
        static let checkMark = "checkMark"
    }
    
    struct CustomerService {
        static let email = "hi@mavrik.money"
    }
    
    struct HTTPHeaders {
        static let authToken = "x-auth-token"
        static let checkTokenRevocation = "x-check-token-revocation"
    }
    
    struct Fonts {
        static let montSerratRegular17 = UIFont.init(name: ("Montserrat-Regular"), size: 17.0)
        static let montSerratRegular14 = UIFont.init(name: ("Montserrat-Regular"), size: 14.0)
        static let montSerratMedium14 = UIFont.init(name: ("Montserrat-Medium"), size: 14.0)
        static let montSerratBold25 = UIFont.init(name: ("Montserrat-Bold"), size: 25.0)
        static let montSerratBold20 = UIFont.init(name: ("Montserrat-Bold"), size: 20.0)
        static let montSerratRegular25 = UIFont.init(name: ("Montserrat-Regular"), size: 25.0)
    }
    
    struct Strings {
        static let resetPassword = "resetPassword"
        static let verifyEmail = "verifyEmail"
        static let oobCode = "oobCode"
    }
    
    struct LoginMethods {
        static let apple = "Apple"
        static let google = "Google"
        static let emailPassword = "EmailPassword"
        static let appleProviderId = "apple.com"
    }
    
    struct LoaderActions {
        static let reauth = "LOGIN"
    }
    
    struct LoaderViews {
        static let setPinVCReAuthSuccess = "setPinVCReAuthSuccess"
        static let loaderName = "loaderName"
    }
    
    struct ProfileSettings {
        static let customerService = "Customer Service"
        static let membership = "Membership Settings"
        static let security = "Security Settings"
        static let legal = "Legal"
        static let logout = "Logout"
        static let notifications = "Notification Settings"
        static let pin = "PIN"
        static let loginActivity = "Login Activity"
        static let biometric = "Face/Touch ID"
    }
    
    struct FCM {
        static let token = "FCMToken"
    }
    
    struct Notifications {
        static let cellId = "notificationCell"
    }
    
    struct Cells {
        static let textCellId = "textCell"
        static let imageCellId = "imageCell"
        static let transactionCellId = "transactionCell"
    }
    
    struct NotificationActions {
        static let verifyEmail = "Verify Email"
        static let reAuthSuccess = "Re-Auth-Success"
        static let loaderRemoved = "loaderRemoved"
    }
    
    struct Geographies {
        static let USA = "USA"
        static let India = "India"
        static let Europe = "Europe"
    }
    
    struct BiometryType {
        static let touch = "Touch ID"
        static let face = "Face ID"
    }
    
    struct Firestore {
        static let usersCollection = "users"
    }
    
    struct FirebaseStorage {
        static let usaStorageBucket = "gs://mavrik-prod-kyc-docs-usa"
        static let indiaStorageBucket = "gs://mavrik-prod-kyc-docs-india"
        static let europeStorageBucket = "gs://mavrik-prod-kyc-docs-europe"
    }
    
    struct KYC {
        static let addressProofFront = "Address Proof Front"
        static let addressProofBack = "Address Proof Back"
        static let nextScreenTitle = "nextScreenTitle"
        static let nextScreenVCId = "nextScreenVCId"
        static let kycDocfileNameExtension = ".jpeg"
    }
}
