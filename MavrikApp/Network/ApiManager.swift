//
//  MainDataApiManager.swift
//  MavrikApp
//
//  Created by AK on 15.11.2020.
//

import Foundation
import Alamofire


struct ApiManager {
    
    static func registerUserInDynamoDb(user: [String: Any]) {
        let uid = user["uid"] as! String
        print("Registering user \(uid) in dynamo db")
        ApiRequest.registerUserInDynamoDb(user: user).request { response in
            print("Registering user in dynamo db response: \(response.debugDescription)")
        }
    }
    
    static func getRequiredKycDocs(country: String, account: String, callback: @escaping(KYCDocs?) -> Void) {
        print("Get required kyc docs for \(country) and \(account)")
        ApiRequest.getRequiredKycDocs(country: country, account: account).request { (response) in
            print("Get required kyc docs response: \(response.debugDescription)")
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(KYCDocs.self, from: jsonData)
                callback(data)
                return
            }
        }
    }
    
    static func getHomeScreenData(callback: @escaping(HomeData?) -> Void) {
        let user = "abc"
        ApiRequest.getHome(user: user).request { response in
            print("response: \(response.debugDescription)")
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(HomeData.self, from: jsonData)
                callback(data)
                return
            }
        }
    }
    
    static func getTerms(callback: @escaping(Data?) -> Void) {
        ApiRequest.getTerms.request { response in
            callback(response.data)
        }
    }
    
    static func getUserData(callback: @escaping(ProfileModel?) -> Void) {
        let user = "abc"
        ApiRequest.getProfile(user: user).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(ProfileModel.self, from: jsonData)
                callback(data)
            }
        }
    }
    
    static func getNotifications(user: String, callback: @escaping([NotificationModel]) -> Void) {
        ApiRequest.getNotifications(user: user).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode([NotificationModel].self, from: jsonData)
                callback(data ?? [])
            }
        }
    }
    
    static func getHomeAccountBalance(account: String, callback: @escaping(HomeAccountModel?) -> Void) {
        let user = "abc"
        ApiRequest.getAccountHome(user: user, account: account).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(HomeAccountModel.self, from: jsonData)
                callback(data)
                return
            }
            callback(nil)
        }
    }
    
    static func getHomeAccountDetal(account: String, callback: @escaping([CryptobalanceModel]) -> Void) {
        let user = "abc"
        ApiRequest.getAccountDetal(user: user, account: account).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(CryptobalanceAccounts.self, from: jsonData)
                if let array = data?.accounts {
                    callback(array)
                    return
                }
            }
            callback([])
        }
    }
    
    static func getAccountTransactions(account: String, callback: @escaping([AccountTransactions]) -> Void) {
        let user = "abc"
        ApiRequest.getAccountTransactions(user: user, account: account).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(AccountTransactionsModel.self, from: jsonData)
                if let array = data?.txns {
                    callback(array)
                    return
                }
            }
            callback([])
        }
    }
    
    static func getLinkedPayments(account: String, callback: @escaping([LinkedPayment]) -> Void) {
        let user = "abc"
        ApiRequest.getAccountPayments(user: user, account: account).request { response in
            if let jsonData = response.data {
                let decoder = JSONDecoder()
                let data = try? decoder.decode(LinkedPaymentsModel.self, from: jsonData)
                if let array = data?.linkedPayments {
                    callback(array)
                    return
                }
            }
            callback([])
        }
    }
    
    static func getQRCode(forAddress address: String, callback: @escaping(Data?) -> Void) {
        ApiRequest.getQrCode(address: address).requestWithoutJSON(callback: { response in
            callback(response.data)
        })
    }
    
    static func addDeposit(account: String, param: [String: Any], callback: @escaping(Bool) -> Void) {
        let user = "abc"
        ApiRequest.depositIntoAccount(user: user, account: account, params: param).request { response in
            if response.response?.statusCode == 200 {
                let dic = response.value as? [String: Any] ?? [:]
                let status = dic["status"] as? Bool ?? false
                print("status: \(status)")
                callback(status)
            } else {
                callback(false)
            }
        }
    }
    
    static func storeNotifications(user: String, params: [String: Any], callback: @escaping(Bool) -> Void) {
        ApiRequest.storeNotifications(user: user, params: params).request { response in
            if response.response?.statusCode == 200 {
                print("Store notifications response: \(String(describing: response))")
                callback(true)
            } else {
                callback(false)
            }
        }
    }
}
