//
//  ApiFileRequest.swift
//  MavrikApp
//
//  Created by AK on 18.11.2020.
//

import Foundation
import Alamofire


struct ApiFileRequest {
    
    private static let baseURL = "http://builder-mock-apis-dev.us-west-2.elasticbeanstalk.com"
    
    static func sendFiles(fromUser user: String, data: [String: Any], callback: @escaping(Bool) -> Void) {
        let headers: HTTPHeaders = ["Content-Type": "multipart/form-data"]
        
        AF.upload(multipartFormData: { (multipart) in
            if let country = data["country"] as? String {
                multipart.append(country.data(using: String.Encoding.utf8)!, withName: "country")
            }
            if let status = data["status"] as? String {
                multipart.append(status.data(using: String.Encoding.utf8)!, withName: "status")
            }
            if let photo = data["statusProof"] as? UIImage {
                multipart.append(photo.pngData()!, withName: "statusProof" , fileName: "file.png", mimeType: "image/png")
            }
        }, to: baseURL + "/\(user)/kyc", method: .post, headers: headers).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .failure(let error):
                print("Error creating user: \(error.localizedDescription)")
                callback(false)
            case .success:
                if response.response?.statusCode == 200 {
                    let dic = response.value as? [String: Any] ?? [:]
                    let result = dic["status"] as? Bool ?? false
                    callback(result)
                } else {
                    callback(false)
                }
            }
        })
    }
}
