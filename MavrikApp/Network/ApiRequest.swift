//
//  ApiRequest.swift
//  MavrikApp
//
//  Created by AK on 10.11.2020.
//

import Foundation
import Alamofire
import FirebaseAuth

enum ApiRequest {
    
    case getRequiredKycDocs(country: String, account: String)
    case registerUserInDynamoDb(user: [String: Any])
    
    case getHome(user: String)
    case getProfile(user: String)
    case getNotifications(user: String)
    
    case getAccountHome(user: String, account: String)
    case getAccountDetal(user: String, account: String)
    case getAccountTransactions(user: String, account: String)
    case getAccountPayments(user: String, account: String)
    
    case depositIntoAccount(user: String, account: String, params: [String: Any])
    case storeNotifications(user: String, params: [String: Any])
    
    case getQrCode(address: String)
    
    case getTerms
    
    private enum ParamType {
        case query, body, path, none
    }
    
    private var method: HTTPMethod {
        switch self {
        case .registerUserInDynamoDb:
            return .post
            
        case .depositIntoAccount:
            return .post
            
        case .storeNotifications:
            return .post
            
        default:
            return .get
        }
    }
    
    private var urlPath: String {
        switch self {
        
        case .getHome(let user):
            return "/users/\(user)/home"
            
        case .getProfile(let user):
            return "/users/\(user)/profile"
            
        case .getNotifications(let user):
            return "/users/\(user)/notifications"
            
        case .getAccountHome(let user, let account):
            return "/users/\(user)/accounts/\(account)/home"
            
        case .getAccountDetal(let user, let account):
            return "/users/\(user)/accounts/\(account)/detail"
            
        case .getAccountTransactions(let user, let account):
            return "/users/\(user)/accounts/\(account)/txns"
            
        case .getAccountPayments(let user, let account):
            return "/users/\(user)/accounts/\(account)/payments"
            
        case .getQrCode(let address):
            return "/qr/\(address)"
            
        case .depositIntoAccount(let user, let account, _):
            return "/users/\(user)/accounts/\(account)/deposit"
            
        case .getRequiredKycDocs(let country, let account):
            return "/requiredKycDocs?country=\(country)&account=\(account)"
            
        case .storeNotifications(let user, _):
            return "/users/\(user)/notifications"
            
        case .registerUserInDynamoDb(let user):
            let uid = user["uid"] as! String
            return "/users/\(uid)/register"
            
        case .getTerms:
            return "/terms"
            
        }
    }
    
    private var parameters: [String: Any] {
        switch self {
        
        case .registerUserInDynamoDb(let params):
            return params
        
        case .depositIntoAccount(_, _, let params):
            return params
            
        case .storeNotifications(_, let params):
            return params
            
        default:
            return [:]
        }
    }
    
    private var headers: HTTPHeaders {
        switch self {
        default:
            return ["Content-Type": "application/json"]
        }
    }
    
    private var paramType: ParamType {
        switch self {
        case .registerUserInDynamoDb:
            return .body
            
        case .depositIntoAccount:
            return .body
            
        case .storeNotifications:
            return .body
            
        default:
            return .path
        }
    }
    
    func request(completion: @escaping (_ response: AFDataResponse<Any>) -> Void) {
        let baseURL = Constants.URL.serverBase
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { token, error in
            if error != nil {
                print("Error occured while getting auth token \(String(describing: error?.localizedDescription))")
            } else {
                // set auth token in header
                var headers = self.headers
                headers[Constants.HTTPHeaders.authToken] = token
                if method == .get {
                    let path = urlPath
                    let url = baseURL + path
                    let urlEncoded = url.stringByAddingPercentEncodingForRFC3986() ?? url
                    AF.request(urlEncoded, method: method, headers: headers).responseJSON { response in
                        completion(response)
                    }
                } else {
                    AF.request(baseURL + urlPath, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        completion(response)
                    }
                }
            }
        }
    }
    
    func requestWithoutJSON(callback: @escaping(AFDataResponse<Data>) -> Void) {
        let baseURL = Constants.URL.serverBase
        let path = urlPath
        let url = baseURL + path
        let urlEncoded = url.stringByAddingPercentEncodingForRFC3986() ?? url
        AF.request(urlEncoded, method: method, headers: headers).responseData { (response) in
            callback(response)
        }
    }
}
