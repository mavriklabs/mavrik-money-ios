//
//  UIViewController+Extension.swift
//  SpecialOffers
//
//  Created by AK on 09.10.2020.
//

import UIKit
import Hero
import FirebaseAuth
import LocalAuthentication
import MessageUI

enum VCIDs: String {
    case SplashVC
    case StartInfoVC
    case SignInVC
    case Step1PhoneVC
    case SearchCountryCodeVC
    case Step2SelectDocumentVC
    case CreateNewAccountVC
    case Step3MembershipVC
    case AddPaymentVC
    case Step3ConfirmType
    case Step3EnterPinVC
    case HomeVC
    case ProfileVC
    case BanksVCNav
    case NotificationVC
    case CryptoAccountVC
    case CryptoAccountVCNav
    case BanksVC
    case NotificationSettingsVC
    case SecuritySettingsVCNav
    case SecuritySettingsVC
    case SetPinVC
    case LoginActivityVC
    case FaceAndTouchIDVC
    case SuccessfulVC
    case DepositVCNav
    case DepositVC
    case CustomerServicesVC
    case ChatVC
    case CameraVC
    case CheckPhotoVC
    case DeleteBankAccountVC
    case ManageMembershipVC
    case MembershipSettingsVC
    case AddDepositVC
    case AddDepositDetail
    case CryptobalanceVC
    case TransactionsVC
    case SetPasswordVC
    case ForgotPasswordVC
    case TermsVC
    case TabbarVC
    case TabbarNavVC
    case VerifyEmailVC
    case TestVC
}

extension UIViewController {
    
    func goToVC(withID id: VCIDs, style: UIModalPresentationStyle) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id.rawValue)
        vc.modalPresentationStyle = style
        self.present(vc, animated: true)
    }
    
    func goToVC(withID id: VCIDs, style: UIModalPresentationStyle, transition: HeroDefaultAnimationType) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id.rawValue)
        vc.hero.isEnabled = true
        vc.modalPresentationStyle = style
        vc.hero.modalAnimationType = transition
        self.present(vc, animated: true)
    }
    
    func goToNextKycCaptureScreen() {
        print("Going to next kyc capture screen")
        let nextScreen = getNextKycDocCaptureScreen()
        let title = nextScreen[Constants.KYC.nextScreenTitle]
        let vcId = nextScreen[Constants.KYC.nextScreenVCId]
        print(nextScreen)
        // present it
        if vcId == VCIDs.CameraVC.rawValue {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: vcId!) as! CameraVC
            vc.vcTitleCurrent = title
            vc.hero.isEnabled = true
            vc.modalPresentationStyle = .fullScreen
            vc.hero.modalAnimationType = HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right))
            self.present(vc, animated: true)
        } else if vcId == VCIDs.Step3ConfirmType.rawValue {
            self.goToVC(withID: .Step3ConfirmType, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
        } else {
            print("Unknown screen returned by getNextKycDocCaptureScreen method")
        }
    }
    
    func getNextKycDocCaptureScreen() -> [String:String] {
        print("Getting next next kyc capture screen")
        var docs: [String] = []
        // Strings in this array are defined in Constants file under KYC struct. 
        docs = LocalStorage().getProfileKey(key: .requiredKycDocs) ?? docs
        print(docs)
        if docs.count > 0 {
            let doc = docs.remove(at: 0)
            // save docs back to local storage
            LocalStorage().setProfileKey(key: .requiredKycDocs, value: docs)
            
            var map: [String:String] = [:]
            map[Constants.KYC.nextScreenVCId] = VCIDs.CameraVC.rawValue
            map[Constants.KYC.nextScreenTitle] = doc
            return map
        } else {
            // no more docs left
            // Update user reg info in firebase and locally
            LocalStorage().setProfileKey(key: .kycDocsSubmitted, value: true)
            let userId = Auth.auth().currentUser!.uid
            let data = [LocalProfileKeys.kycDocsSubmitted.rawValue : true]
            CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
            
            return [Constants.KYC.nextScreenVCId: VCIDs.Step3ConfirmType.rawValue]
        }
    }
    
    func saveImage(imageName: String, image: UIImage) {
        print("Saving image in firebase")
        let fileName = imageName
        let data = image.jpegData(compressionQuality: 1)
        if data == nil {
            print("Image data is nil")
        } else {
            // store image in firebase
            let bucket = CustomUtils.getFirebaseStorageBucketForUser()
            let folder = Auth.auth().currentUser!.uid
            print("Storing in bucket \(bucket) and folder \(folder) with filename \(fileName)")
            CustomUtils.uploadFileToFirebase(bucket: bucket, folder: folder, fileName: fileName, data: data!)
        }
    }
    
    func showAlert(withText text: String) {
        let alert = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        alert.setTitle(font: Constants.Fonts.montSerratMedium14, color: nil)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(email: String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
        return pred.evaluate(with: email)
    }
    
    func isValidPassword(pass: String?) -> Bool {
        guard pass != nil else { return false }
        // at least one uppercase
        // at least one digit
        // at least one lowercase
        // at least one special character
        // 8 characters minimum
        //let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?=.*[$@$#!%*?&]).{8,}")
        
        // at least one uppercase
        // at least one lowercase
        // min 8 characters
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: pass)
    }
    
    func showBiometryDeniedAlert() {
        print("Denied biometry access")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "\(self.getBiometryType()) access denied" , message: "Please allow access in settings",  preferredStyle: .alert)
            let deny = UIAlertAction(title: "Deny", style: .default)
            let settings = UIAlertAction(title: "Settings", style: .default) { action in
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                if let settingsUrl = settingsUrl, UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl)
                }
            }
            alert.addAction(deny)
            alert.addAction(settings)
            self.present(alert, animated: true)
        }
    }
    
    func startActivityAnimation(activityColor: UIColor, backgroundColor: UIColor, indicatorScale: CGFloat) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .medium
        activityIndicator.color = activityColor
        activityIndicator.transform = CGAffineTransform.init(scaleX: indicatorScale, y: indicatorScale)
        
        activityIndicator.startAnimating()
        self.view.isUserInteractionEnabled = false
        backgroundView.addSubview(activityIndicator)
        self.view.addSubview(backgroundView)
    }
    
    func stopActivityAnimation() {
        if let background = view.viewWithTag(475647) {
            background.removeFromSuperview()
        }
        self.view.isUserInteractionEnabled = true
    }
}

// MARK:- Biometry

extension UIViewController {
    
    func getBiometryType() -> String {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return ""
            case .touchID:
                return Constants.BiometryType.touch
            case .faceID:
                return Constants.BiometryType.face
            @unknown default:
                return ""
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? "Touch ID" : ""
        }
    }
    
    func authenticateWithBiometrics(completion: @escaping (Bool?, Int?) -> ()) {
        let localAuthenticationContext = LAContext()
        var authError: NSError?
        let biometryType = getBiometryType()
        let reason = "\(biometryType) is required to perform this action"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, error in
                if success {
                    print("Biometry auth successful")
                    completion(true, nil)
                } else {
                    if let error = error {
                        print("Biometry auth failed with error:")
                        print(self.parseBiometricsError(errorCode: error._code))
                        completion(false, error._code)
                    } else {
                        print("Biometry auth failed with nil error")
                        completion(false, nil)
                    }
                }
            }
        } else {
            if let error = authError {
                print("Cannot evaluate policy. Error is:")
                print(self.parseBiometricsError(errorCode: error.code))
                completion(false, error.code)
            } else {
                print("Cannot evaluate policy. Operation failed with nil error")
                completion(false, nil)
            }
        }
    }
    
    func parseBiometricsError(errorCode: Int) -> String {
        var message = ""
        switch errorCode {
        case LAError.biometryNotAvailable.rawValue:
            message = "Authentication could not start because user has denied biometric authentication"
        case LAError.biometryLockout.rawValue:
            message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times"
        case LAError.biometryNotEnrolled.rawValue:
            message = "Authentication could not start because the user has not enrolled in biometric authentication"
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
        case LAError.userCancel.rawValue:
            message = "Cancelled by user"
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
        default:
            message = "Did not find error code on LAError object"
        }
        return message
    }
}

// MARK:- Email sender extension

extension UIViewController {
    
    func sendEmailToSupport() {
        let supportEmail = Constants.CustomerService.email
        let sender = LocalStorage().getProfileKey(key: .name) ?? ""
        let subject = "Send help!!!"
        let body = "Hi, \n\nI am \(sender). I am trying to ...  \n\nPlease help. Thanks."
        
        if let emailUrl = createEmailUrl(to: supportEmail, subject: subject, body: body) {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        return defaultUrl
    }
}
