//
//  Utils.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/4/20.
//

import CryptoKit
import Foundation
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage

struct CustomUtils {
    
    static func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        return result
    }
    
    static func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        return hashString
    }
    
    static func fetchAndStoreFirebaseUserProfile(completion: @escaping ([String:Any]?)->()) {
        let userId = Auth.auth().currentUser?.uid
        if let userId = userId {
            getFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId) { data in
                if let data = data {
                    for key in LocalProfileKeys.allCases {
                        let datum = data[key.rawValue]
                        print("Writing \(key) to local storage with value \(datum.debugDescription)")
                        LocalStorage().setProfileKey(key: key, value: datum)
                    }
                    completion(data)
                } else {
                    // firebase user profile is nil, remove profile from local storage
                    LocalStorage().removeLocalProfile()
                    completion(nil)
                }
            }
        }
    }
    
    static func registerUserInFirebaseAndDynamoDb(user: [String: Any]) {
        let db = Firestore.firestore()
        var data = user
        data["createdAt"] = FieldValue.serverTimestamp()
        let uid = user["uid"] as! String
        print("Registering user \(uid) in firebase")
        db.collection(Constants.Firestore.usersCollection).document(uid).setData(data, merge: true) { err in
            if let err = err {
                print("Error storing user registration info in firestore: \(err.localizedDescription)")
            } else {
                // register user in dynamodb
                ApiManager.registerUserInDynamoDb(user: user)
            }
        }
    }
    
    static func updateFirestoreDoc(collection: String, docId: String, data: [String:Any]) {
        print("Updating firebase doc: \(docId)")
        let db = Firestore.firestore()
        var data = data
        data["updatedAt"] = FieldValue.serverTimestamp()
        db.collection(collection).document(docId).setData(data, merge: true) { err in
            if let err = err {
                print("Error updating firestore doc: \(err.localizedDescription)")
            }
        }
    }
    
    static func getFirestoreDoc(collection: String, docId: String, completion: @escaping ([String:Any]?)->()) {
        print("Getting doc with id: \(docId)")
        let db = Firestore.firestore()
        let docRef = db.collection(collection).document(docId)
        docRef.getDocument { (document, error) in
            if let error = error {
                print("Error getting firestore doc: \(error.localizedDescription)")
                print(error)
            }
            if let document = document, document.exists {
                completion(document.data())
            } else {
                completion(nil)
            }
        }
    }
    
    static func getScreenToPresent() -> VCIDs {
        //return .TestVC
        print("Getting screen to present")
        var vcId = VCIDs.SignInVC
        let user = Auth.auth().currentUser ?? nil
        if let user = user {
            print("Logged in user exists")
            // check if phone exists locally
            let phone = LocalStorage().getProfileKey(key: .phone) ?? ""
            
            // check if kyc docs are submitted
            let kycDocsSubmitted = LocalStorage().getProfileKey(key: .kycDocsSubmitted) ?? false
            
            // check if 2fa is set
            let multiFactorSet = LocalStorage().getProfileKey(key: .multiFactorSet) ?? false
            
            // check if user verified email
            let emailVerified = user.isEmailVerified
            
            print("Phone: \(phone), KYC submitted: \(kycDocsSubmitted), multi factor auth set: \(multiFactorSet), email verified: \(emailVerified)")
            
            if phone == "" {
                vcId = .StartInfoVC
            } else if !kycDocsSubmitted {
                vcId = .Step2SelectDocumentVC
            } else if !multiFactorSet {
                vcId = .Step3ConfirmType
            } else if !emailVerified {
                vcId = .VerifyEmailVC
            } else {
                vcId = .TabbarNavVC
            }
        } else {
            vcId = .SignInVC
        }
        return vcId
    }
    
    static func loadImageFromDiskWith(fileName: String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
        }
        return nil
    }
    
    static func sendVerificationEmail(completion: @escaping (Void?)->()) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = Constants.URL.base
        urlComponents.path = Constants.URL.emailActionsPath
        
        let actionCodeSettings =  ActionCodeSettings.init()
        actionCodeSettings.handleCodeInApp = true
        actionCodeSettings.url = urlComponents.url
        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
        actionCodeSettings.dynamicLinkDomain = Constants.URL.fDLDomain
        
        Auth.auth().currentUser?.sendEmailVerification(with: actionCodeSettings) { error in
            if (error != nil) {
                print("Firebase sending verification email error: \(error!.localizedDescription)")
                return
            }
            completion(nil)
        }
    }
    
    static func uploadFileToFirebase(bucket: String, folder: String, fileName: String, data: Data) {
        let storageRef = Storage.storage(url: bucket).reference()
        let folderRef = storageRef.child(folder)
        let fileRef = folderRef.child(fileName)
        // Upload the file
        fileRef.putData(data, metadata: nil) { (metadata, error) in
            if let error = error {
                print("Error occured in uploading file: \(error.localizedDescription)")
                return
            }
            guard metadata != nil else {
                print("Error occured in uploading file")
                return
            }
            print("File \(fileName) uploaded successfully to folder \(folder) in bucket \(bucket)")
        }
    }
    
    static func getFirebaseStorageBucketForUser() -> String {
        // get residence country
        let country = LocalStorage().getProfileKey(key: .residenceCountry) ?? ""
        if country == "" {
            print("No country found for user. Getting default bucket")
            return Constants.FirebaseStorage.usaStorageBucket
        } else if country == Constants.Geographies.USA {
            return Constants.FirebaseStorage.usaStorageBucket
        } else if country == Constants.Geographies.India {
            return Constants.FirebaseStorage.indiaStorageBucket
        } else if country == Constants.Geographies.Europe {
            return Constants.FirebaseStorage.europeStorageBucket
        } else {
            return Constants.FirebaseStorage.europeStorageBucket
        }
    }
    
    static func getSHA256(for string : String) -> String {
        let inputData = Data(string.utf8)
        let hashed = SHA256.hash(data: inputData)
        let hashString = hashed.compactMap { String(format: "%02x", $0) }.joined()
        return hashString
    }
}
