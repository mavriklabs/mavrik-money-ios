//
//  CountryCodeParser.swift
//  MavrikApp
//
//  Created by AK on 21.11.2020.
//

import Foundation

struct CountryCodeParser {
    
    private static func readLocalFile() -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: "countryCodes",
                                                 ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    
    static func getData(callback: ([CountryCode]) -> Void) {
        guard let jsonData = readLocalFile() else {
            callback([])
            return
        }
        
        do {
            let decodedData = try JSONDecoder().decode([CountryCode].self,
                                                       from: jsonData)
            callback(decodedData)
        } catch {
            callback([])
            print("decode error")
        }
    }
    
}
