//
//  LoaderAnimationManager.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import UIKit
import Lottie

protocol LoaderAnimationProtocol {
    func showLoader(withText text: String)
    func removeLoader(withAnimation: AnimationType)
}

class LoaderAnimation: LoaderAnimationProtocol {
    
    private let vc: UIViewController!
    private var dismissAnimationType: AnimationType!
    
    init(vc: UIViewController) {
        self.vc = vc
        self.dismissAnimationType = .popOut
    }
    
    func addGestureRecognition(to view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideLoader))
        tap.cancelsTouchesInView = false
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(hideLoader))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        view.addGestureRecognizer(swipe)
    }
    
    func showLoader() {
        showLoader(withText: "")
    }
    
    func showLoaderWithDone(withText text: String, view: LoaderView, done: Bool) {
        view.frame = vc.view.frame
        view.configLoaderViewWithDone(withText: text, done: done)
        // if done, view is already loaded so no need to add subview again
        if !done {
            rotateView(targetView: view.loaderIcon, duration: 0.33)
            vc.view.addSubview(view)
            Animations.animateView(type: .fadeIn, view: view.bgView, vc: vc, completion: {})
            Animations.animateView(type: .popIn, view: view.popupView, vc: vc, completion: {})
        } else {
            checkMarkAnimation(view)
        }
    }
    
    func showLoader(withText text: String) {
        let view = LoaderView().loadNib() as! LoaderView
        view.frame = vc.view.frame
        view.configLoaderView(withText: text)
        rotateView(targetView: view.loaderIcon, duration: 0.33)
        vc.view.addSubview(view)
        Animations.animateView(type: .fadeIn, view: view.bgView, vc: vc, completion: {})
        Animations.animateView(type: .buttonTap, view: view.popupView, vc: vc, completion: {})
    }
    
    @objc func hideLoader() {
        removeLoader(withAnimation: dismissAnimationType)
    }
    
    func removeLoader(withAnimation: AnimationType) {
        DispatchQueue.main.async {
            for view in self.vc.view.subviews {
                if view is LoaderView {
                    let loaderView = view as! LoaderView
                    Animations.animateView(type: .fadeOut, view: loaderView.bgView, vc: self.vc, completion: {})
                    Animations.animateView(type: withAnimation, view: loaderView.popupView, vc: self.vc) {
                        loaderView.removeFromSuperview()
                        //NotificationCenter.default.post(name: NSNotification.Name.init(Constants.NotificationActions.loaderRemoved), object: nil, userInfo: [Constants.LoaderViews.loaderName : loaderView.getName()])
                    }
                }
            }
        }
    }
    
    func showMessage(for loaderView: String, withText text: String) {
        showMsg(loaderView, text)
    }
    
    func showMessage(withText text: String) {
        showMsg("", text)
    }
    
    private func showMsg(_ viewName: String, _ text: String) {
        let view = LoaderView().loadNib() as! LoaderView
        addGestureRecognition(to: view)
        view.frame = vc.view.frame
        dismissAnimationType = .popOut
        view.configMessageView(withText: text)
        view.setName(viewName)
        vc.view.addSubview(view)
        Animations.animateView(type: .fadeIn, view: view.bgView, vc: vc, completion: {})
        Animations.animateView(type: .popIn, view: view.popupView, vc: vc, completion: {})
    }
    
    func showLogoutPrompt() {
        let loaderView = LoaderView().loadNib() as! LoaderView
        addGestureRecognition(to: loaderView)
        loaderView.frame = vc.view.frame
        dismissAnimationType = .toBottom
        loaderView.okButton.addTarget(self, action: #selector(logoutDidTap), for: .touchUpInside)
        loaderView.configLogoutPrompt()
        vc.view.addSubview(loaderView)
        Animations.animateView(type: .fadeIn, view: loaderView.bgView, vc: vc, completion: {})
        Animations.animateView(type: .fromBottom, view: loaderView.popupView, vc: vc, completion: {})
    }
    
    func showActionPrompt(with action: String, text: String) {
        let loaderView = LoaderView().loadNib() as! LoaderView
        addGestureRecognition(to: loaderView)
        loaderView.frame = vc.view.frame
        dismissAnimationType = .popOut
        loaderView.okButton.addTarget(self, action: #selector(actionDidTap(_:)), for: .touchUpInside)
        loaderView.configActionPrompt(with: action, text: text)
        vc.view.addSubview(loaderView)
        Animations.animateView(type: .fadeIn, view: loaderView.bgView, vc: vc, completion: {})
        Animations.animateView(type: .popIn, view: loaderView.popupView, vc: vc, completion: {})
    }
    
    @objc private func logoutDidTap() {
        let vc = self.vc as? ProfileVC
        vc?.logout()
    }
    
    @objc private func actionDidTap(_ actionButton: UIButton) {
        switch actionButton.currentTitle {
        case Constants.LoaderActions.reauth:
            let vc = self.vc as? SetPinVC
            vc?.reauth()
        default :
            print("No action found")
        }
    }
    
    private func checkMarkAnimation(_ view: LoaderView) {
        let animationView = AnimationView.init(name: Constants.LottieAnimatedIcons.checkMark)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .playOnce
        animationView.animationSpeed = 1
        
        view.loaderIcon.isHidden = true
        view.lottieView.isHidden = false
        animationView.frame = view.lottieView.frame
        view.loaderStack.addSubview(animationView)
        animationView.play()
    }
    
    func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: .pi)
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
}
