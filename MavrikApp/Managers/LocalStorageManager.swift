//
//  LocalStorageManager.swift
//  MavrikApp
//
//  Created by AK on 15.11.2020.
//

import Foundation

enum LocalAuthKeys: String, CaseIterable {
    case mfaTypePin, mfaTypeFaceId, mfaTypeTouchId, pin, phoneVerificationId
}

enum LocalProfileKeys: String, CaseIterable {
    case name, givenName, email, phone, address, registrationComplete,
         requiredKycDocs, residenceCountry, residenceCountryCode, kycDocsSubmitted, multiFactorSet, loginMethod, photoURL,
         pushNotifications
    
}

enum LocalAuthType: String {
    case pin, password, faceId, touchId, none
}

class LocalStorage {
    
    private let manager = UserDefaults.init(suiteName: "Mavrik")!
    
    init() {}
    
    func getAuthKey<T>(key: LocalAuthKeys) -> T? {
        return manager.object(forKey: key.rawValue) as? T
    }
    
    func setAuthKey<T>(key: LocalAuthKeys, value: T?) {
        manager.set(value, forKey: key.rawValue)
    }
    
    func getProfileKey<T>(key: LocalProfileKeys) -> T? {
        return manager.object(forKey: key.rawValue) as? T
    }
    
    func setProfileKey<T>(key: LocalProfileKeys, value: T?) {
        manager.set(value, forKey: key.rawValue)
    }
    
    func removeProfileKey(key: LocalProfileKeys) {
        manager.set(nil, forKey: key.rawValue)
    }
    
    func removeAuthKey(key: LocalAuthKeys) {
        manager.set(nil, forKey: key.rawValue)
    }
    
    func removeLocalProfile() {
        print("Removing local profile")
        for key in LocalProfileKeys.allCases {
            removeProfileKey(key: key)
        }
    }
    
    func removeAllData() {
        let domain = Bundle.main.bundleIdentifier!
        manager.removePersistentDomain(forName: domain)
    }
}
