//
//  SocialLogin.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/4/20.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import AuthenticationServices
import CryptoKit
import Hero

class SocialLogin: NSObject {
    
    private let vc: UIViewController!
    private var loaderAnimation: LoaderAnimation!
    private var currentNonce: String?
    private let storage = LocalStorage()
    private var isSignUpFlow = false
    private var isReauthFlow = false
    
    init(vc: UIViewController, loaderAnimation: LoaderAnimation, isSignUpFlow: Bool) {
        self.vc = vc
        self.loaderAnimation = loaderAnimation
        self.isSignUpFlow = isSignUpFlow
    }
    
    init(vc: UIViewController, loaderAnimation: LoaderAnimation, isSignUpFlow: Bool, isReauthFlow: Bool) {
        self.vc = vc
        self.loaderAnimation = loaderAnimation
        self.isSignUpFlow = isSignUpFlow
        self.isReauthFlow = isReauthFlow
    }
    
    private func goToVC(withID id: VCIDs, style: UIModalPresentationStyle, transition: HeroDefaultAnimationType) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id.rawValue)
        vc.hero.isEnabled = true
        vc.modalPresentationStyle = style
        vc.hero.modalAnimationType = transition
        self.vc.present(vc, animated: true)
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
}

extension SocialLogin: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        self.loaderAnimation.showLoader(withText: "Logging in")
        if let error = error {
            self.loaderAnimation.removeLoader(withAnimation: .popOut)
            print("Google auth errored: \(error.localizedDescription)")
            return
        }
        
        guard let authentication = user.authentication else {
            self.loaderAnimation.removeLoader(withAnimation: .popOut)
            print("Google auth user authentication is nil")
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        // Sign in with Firebase.
        firebaseSignIn(name: nil, withProvider: Constants.LoginMethods.google, credential: credential)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
}

extension SocialLogin: ASAuthorizationControllerPresentationContextProviding, ASAuthorizationControllerDelegate {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.vc.view.window!
    }
    
    func startSignInWithAppleFlow() {
        let nonce = CustomUtils.randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = CustomUtils.sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Logging in with Apple errored. " + error.localizedDescription)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            self.loaderAnimation.showLoader(withText: "Logging in")
            guard let nonce = currentNonce else {
                self.loaderAnimation.removeLoader(withAnimation: .popOut)
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                self.loaderAnimation.removeLoader(withAnimation: .popOut)
                print("Unable to fetch identity token")
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                self.loaderAnimation.removeLoader(withAnimation: .popOut)
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            // Get name
            let fullNameObj = appleIDCredential.fullName
            let givenName = fullNameObj?.givenName
            let middleName = fullNameObj?.middleName
            let familyName = fullNameObj?.familyName
            let name = [givenName, middleName, familyName].compactMap { $0 }.joined(separator: " ")
            
            // Initialize a Firebase credential.
            let credential = OAuthProvider.credential(withProviderID: Constants.LoginMethods.appleProviderId, idToken: idTokenString, rawNonce: nonce)
            
            // Sign in with Firebase.
            firebaseSignIn(name: name, withProvider: Constants.LoginMethods.apple, credential: credential)
        }
    }
    
    func firebaseSignIn(name: String?, withProvider: String, credential: AuthCredential) {
        if isReauthFlow {
            Auth.auth().currentUser?.reauthenticate(with: credential) { (authResult, error) in
                if let error = error {
                    self.handleError(error)
                    return
                }
                print("Re-Auth success")
                self.loaderAnimation.removeLoader(withAnimation: .popOut)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationActions.reAuthSuccess), object: nil)
            }
        } else {
            signIn(name: name, withProvider: withProvider, credential: credential)
        }
    }
    
    func signIn(name: String?, withProvider: String, credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                self.handleError(error)
                return
            }
            if let user = Auth.auth().currentUser {
                let email = user.email ?? ""
                if email == "" {
                    self.loaderAnimation.showMessage(withText: "Email is empty")
                    return
                }
                // fetch and store firebase user profile data locally
                CustomUtils.fetchAndStoreFirebaseUserProfile() { doc in
                    if self.isSignUpFlow || doc == nil {
                        let photoURL = user.photoURL?.absoluteString ?? ""
                        var userInfo: [String: Any] = [:]
                        let name = name ?? user.displayName
                        if name != "" {
                            userInfo[LocalProfileKeys.name.rawValue] = name
                        }
                        userInfo["uid"] = user.uid
                        userInfo[LocalProfileKeys.email.rawValue] = email
                        userInfo[LocalProfileKeys.photoURL.rawValue] = photoURL
                        userInfo["emailVerified"] = user.isEmailVerified
                        userInfo[LocalProfileKeys.loginMethod.rawValue] = withProvider
                        CustomUtils.registerUserInFirebaseAndDynamoDb(user: userInfo)
                        
                        //store locally
                        LocalStorage().setProfileKey(key: .photoURL, value: photoURL)
                        LocalStorage().setProfileKey(key: .name, value: name)
                        LocalStorage().setProfileKey(key: .email, value: email)
                        LocalStorage().setProfileKey(key: .loginMethod, value: withProvider)
                    }
                    print("User \(String(describing: name)) logged in with \(withProvider) email \(email)")
                    let vcId = CustomUtils.getScreenToPresent()
                    self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
                }
            }
        }
    }
}
