//
//  CustomFont.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit

enum CustomFont: String {
    case black = "Montserrat-Black"
    case bold = "Montserrat-Bold"
    case light = "Montserrat-Light"
    case medium = "Montserrat-Medium"
    case regular = "Montserrat-Regular"
    case thin = "Montserrat-Thin"
    func of(size:Int) -> UIFont {
        return UIFont(name: self.rawValue, size: CGFloat(size))!
    }
}
