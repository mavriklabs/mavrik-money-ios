//
//  SceneDelegate.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit
import FirebaseDynamicLinks

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        let incomingURL = userActivity.webpageURL ?? nil
        if incomingURL != nil {
            print("User activity has incoming url: \(incomingURL!.absoluteString)")
            handleIncomingLink(incomingURL!)
        } else {
            // handle other user activity types
        }
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)

        self.presentVC(id: VCIDs.SplashVC.rawValue)
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }
    
    fileprivate func handleIncomingLink(_ incomingURL: URL) {
        let incomingURLWithSlash = addTrailingSlash(toURL: incomingURL)
        let isDynamicLink = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURLWithSlash) { (dynamicLink, error) in
            print("Dynamic link call returned")
            if error != nil  {
                print("Error in handling incoming link \(incomingURLWithSlash): \(error?.localizedDescription ?? "")")
                return
            }
            if dynamicLink != nil {
                self.handleIncomingDynamicLink(dynamicLink!)
            } else {
                print("Incoming link has no dynamic link")
            }
        }
        if !isDynamicLink {
            // handle other types of links
            print("Incoming link is not a dynamic link")
            return
        }
    }
    
    fileprivate func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        let url = dynamicLink.url
        if url == nil {
            print("Dynamic link object has no url")
            return
        }
        print("Handling Dynamic link \(url!.absoluteString)")
        let queryParams = URL(string: url!.absoluteString)!.params()
        if url!.path == Constants.URL.emailActionsPath {
            let mode = queryParams["mode"] ?? nil
            if mode != nil {
                let modeStr = mode as! String
                print("Mode is \(modeStr)")
                if modeStr == Constants.Strings.resetPassword {
                    // reset password flow
                    let oobCode = queryParams[Constants.Strings.oobCode] as! String
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.SetPasswordVC.rawValue) as? SetPasswordVC
                    vc?.setPassResetCode(oobCode)
                    window?.rootViewController = vc
                    window?.makeKeyAndVisible()
                } else if modeStr == Constants.Strings.verifyEmail {
                    // verify email flow
                    let oobCode = queryParams[Constants.Strings.oobCode] as! String
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.VerifyEmailVC.rawValue) as? VerifyEmailVC
                    vc?.setVerifyEmailCode(oobCode)
                    vc?.setShouldVerifyEmail(true)
                    window?.rootViewController = vc
                    window?.makeKeyAndVisible()
                }
            }
        }
    }
    
    private func addTrailingSlash(toURL: URL) -> URL {
        let host = toURL.host!
        let path = toURL.path
        // add slash only for fdl domains with empty paths
        if (host == Constants.URL.fDLDomain && path == "") {
            let newStr = toURL.absoluteString.replacingOccurrences(of: host, with: host + "/")
            let newUrl = URL(string: newStr)
            return newUrl!
        }
        // else return original
        return toURL
    }
    
    private func presentVC(id: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
    }
}
