//
//  AnimationEnums.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/18/20.
//

import Foundation

public enum AnimationType {
    case fromLeft
    case fromRight
    case fromTop
    case fromBottom
    case fadeIn
    case fadeOut
    case popIn
    case popOut
    case shake
    case buttonTap
    case toLeft
    case toRight
    case toTop
    case toBottom
}

public enum CellAnimationType: String {
    case alpha
    case wave
    case leftToRight
    case topToBottom
    case bounce
    case rightToLeft
    case rotate
    case linear
    case zoom
    case cardDrop
    case dragFromRight
}

public enum HorizonType {
    case left
    case right
    case top
    case bottom
}

