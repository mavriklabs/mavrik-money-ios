//
//  Animations.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/18/20.
//

import UIKit

public class Animations {
    
    public static func animateView(type: AnimationType, duration: TimeInterval, view: UIView, vc: UIViewController, completion: @escaping () -> Void) {
        switch type {
        case .fromLeft:
            showAnimation(type: .left, duration: duration, forView: view, inVC: vc, completion: {completion()})
        case .fromRight:
            showAnimation(type: .right, duration: duration, forView: view, inVC: vc, completion: {completion()})
        case .fromTop:
            showAnimation(type: .top, duration: duration, forView: view, inVC: vc, completion: {completion()})
        case .fromBottom:
            showAnimation(type: .bottom, duration: duration, forView: view, inVC: vc, completion: {completion()})
        case .fadeIn:
            fadeInAnimation(forView: view, completion: {completion()})
        case .fadeOut:
            fadeOutAnimation(forView: view, completion: {completion()})
        case .popIn:
            popInAnimation(forView: view, completion: {completion()})
        case .popOut:
            popOutAnimation(forView: view, completion: {completion()})
        case .shake:
            shake(view: view)
            completion()
        case .buttonTap:
            buttonTapAnimation(view: view, completion: {completion()})
        case .toLeft:
            hideAnimation(type: .left, forView: view, inVC: vc, completion: {completion()})
        case .toRight:
            hideAnimation(type: .right, forView: view, inVC: vc, completion: {completion()})
        case .toBottom:
            hideAnimation(type: .bottom, forView: view, inVC: vc, completion: {completion()})
        case .toTop:
            hideAnimation(type: .top, forView: view, inVC: vc, completion: {completion()})
        }
    }
    
    public static func animateView(type: AnimationType, view: UIView, vc: UIViewController, completion: @escaping () -> Void) {
        animateView(type: type, duration: 0.5, view: view, vc: vc, completion: {completion()})
    }
    
    public static func cellAnimation(type: CellAnimationType, cell: UITableViewCell, indexPath: IndexPath, completion: @escaping () -> Void) {
        switch type {
        case .alpha:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .alpha, completion: {completion()})
        case .bounce:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .bounce, completion: {completion()})
        case .cardDrop:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .cardDrop, completion: {completion()})
        case .dragFromRight:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .dragFromRight, completion: {completion()})
        case .leftToRight:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .leftToRight, completion: {completion()})
        case .linear:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .linear, completion: {completion()})
        case .rightToLeft:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .rightToLeft, completion: {completion()})
        case .rotate:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .rotate, completion: {completion()})
        case .topToBottom:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .topToBottom, completion: {completion()})
        case .wave:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .wave, completion: {completion()})
        case .zoom:
            cellAnimation(forCell: cell, indexPath: indexPath, animationType: .zoom, completion: {completion()})
        }
    }
}

extension Animations {
    
    private static func getOffset(type: HorizonType, forView animatedView: UIView, inVC vc: UIViewController) -> CGPoint {
        var offset: CGPoint
        switch type {
        case .left:
            offset = CGPoint(x: -vc.view.frame.maxX, y: 0)
            offset.x += animatedView.frame.minX
        case .right:
            offset = CGPoint(x: vc.view.frame.maxX, y: 0)
            offset.x -= animatedView.frame.minX
        case .top:
            offset = CGPoint(x: 0, y: -vc.view.frame.maxY)
            offset.y += animatedView.frame.minY
        case .bottom:
            offset = CGPoint(x: 0, y: vc.view.frame.maxY)
            offset.y -= animatedView.frame.minY
        }
        return offset
    }
    
    private static func showAnimation(type: HorizonType, duration: TimeInterval, forView animatedView: UIView, inVC vc: UIViewController, completion: @escaping () -> Void) {
        let offset = getOffset(type: type, forView: animatedView, inVC: vc)
        animatedView.transform = CGAffineTransform(translationX: offset.x, y: offset.y)
        animatedView.alpha = 0
        UIView.animate(
            withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear,
            animations: {
                animatedView.transform = .identity
                animatedView.alpha = 1
            })
        { _ in
            completion()
        }
    }
    
    private static func hideAnimation(type: HorizonType, forView animatedView: UIView, inVC vc: UIViewController, completion: @escaping () -> Void) {
        let offset = getOffset(type: type, forView: animatedView, inVC: vc)
        animatedView.transform = .identity
        animatedView.alpha = 1
        UIView.animate(
            withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: .curveLinear,
            animations: {
                animatedView.transform = CGAffineTransform(translationX: offset.x, y: offset.y)
                animatedView.alpha = 0
            })
        { _ in
            completion()
        }
    }
    
    private static func fadeInAnimation(forView animatedView: UIView, completion: @escaping () -> Void) {
        animatedView.alpha = 0
        UIView.animate(
            withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0,
            options: .curveLinear, animations: {
                animatedView.alpha = 0.5
            })  { _ in
            completion()
        }
    }
    
    private static func fadeOutAnimation(forView animatedView: UIView, completion: @escaping () -> Void) {
        animatedView.alpha = 0.5
        UIView.animate(
            withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0,
            options: .curveLinear, animations: {
                animatedView.alpha = 0
            })  { _ in
            completion()
        }
    }
    
    private static func popInAnimation(forView animatedView: UIView, completion: @escaping () -> Void) {
        animatedView.alpha = 0
        animatedView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(
            withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0,
            options: .curveEaseOut, animations: {
                animatedView.transform = .identity
                animatedView.alpha = 1
            })
        { _ in
            completion()
        }
    }
    
    private static func popOutAnimation(forView animatedView: UIView, completion: @escaping () -> Void) {
        animatedView.alpha = 1
        animatedView.transform = .identity
        UIView.animate(
            withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0,
            options: .curveEaseOut, animations: {
                animatedView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                animatedView.alpha = 0
            })
        {_ in
            completion()
        }
    }
    
    private static func startLogoAnimation(forView animatedView: UIView, completion: @escaping () -> Void) {
        animatedView.alpha = 0
        animatedView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(
            withDuration: 1.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0,
            options: .curveEaseOut, animations: {
                animatedView.transform = .identity
                animatedView.alpha = 1
            }) { _ in
            completion()
        }
    }
    
    private static func shake(view: UIView) {
        let midX = view.center.x
        let midY = view.center.y
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: midX - 10, y: midY)
        animation.toValue = CGPoint(x: midX + 10, y: midY)
        view.layer.add(animation, forKey: "position")
    }
    
    private static func buttonTapAnimation(view: UIView, completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.1,
                       animations: {
                        view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                       },
                       completion: { _ in
                        UIView.animate(withDuration: 0.1) {
                            view.transform = CGAffineTransform.identity
                        }
                        completion()
                       })
    }
    
    private  static func cellAnimation(forCell cell: UITableViewCell, indexPath: IndexPath, animationType: CellAnimationType, completion: @escaping () -> Void) {
        switch animationType {
        
        case .alpha:
            cell.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
                cell.alpha = 1
            }) { _ in completion()}
            
        case .wave:
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: 0)
            UIView.animate(withDuration: 4, delay: 0.05 * Double(indexPath.row), usingSpringWithDamping: 0.4, initialSpringVelocity: 0.1,
                           options: .curveEaseIn, animations: {
                            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
                           }) { _ in completion()}
            
        case .leftToRight:
            cell.transform = CGAffineTransform(translationX: 0, y: cell.contentView.frame.height)
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
                cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
            }) { _ in completion()}
            
        case .topToBottom:
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: 0)
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
                cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
            }) { _ in completion()}
            
        case .bounce:
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: 0)
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), usingSpringWithDamping: 0.4, initialSpringVelocity: 0.1,
                           options: .curveEaseIn, animations: {
                            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
                           }) { _ in completion()}
            
        case .rightToLeft:
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: 0)
            UIView.animate(
                withDuration: 0.5,
                delay: 0.05 * Double(indexPath.row),
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                }) { _ in completion()}
            
        case .rotate:
            cell.transform = CGAffineTransform(rotationAngle: 360)
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
                cell.transform = CGAffineTransform(rotationAngle: 0.0)
            }) { _ in completion()}
            
        case .linear:
            cell.transform = CGAffineTransform(translationX: cell.contentView.frame.width, y: cell.contentView.frame.height)
            UIView.animate(
                withDuration: 0.5,
                delay: 0.05 * Double(indexPath.row),
                options: [.curveLinear],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                }) { _ in completion()}
            
        case .zoom:
            cell.transform = CGAffineTransform(scaleX: 0, y : 0)
            UIView.animate(withDuration: 0.5, animations: {
                cell.transform = CGAffineTransform(scaleX: 1, y : 1)
            }) { _ in completion()}
            
        case .dragFromRight:
            cell.center.x += 200
            UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), animations: {
                cell.center.x -= 200
            }) { _ in completion()}
            
        case .cardDrop:
            let view = cell.contentView
            view.layer.transform = Animations.TipInCellAnimatorStartTransform
            view.layer.opacity = 0.8
            UIView.animate(withDuration: 0.5, animations: {
                view.layer.transform = CATransform3DIdentity
                view.layer.opacity = 1
            }) { _ in completion()}
        }
    }
    
    private static let TipInCellAnimatorStartTransform: CATransform3D = {
        let rotationDegrees: CGFloat = -15.0
        let rotationRadians: CGFloat = rotationDegrees * (CGFloat(Double.pi)/180.0)
        let offset = CGPoint(x: -20, y: -20)
        var startTransform = CATransform3DIdentity
        startTransform = CATransform3DRotate(CATransform3DIdentity, rotationRadians, 0.0, 0.0, 1.0)
        startTransform = CATransform3DTranslate(startTransform, offset.x, offset.y, 0.0)
        return startTransform
    }()
}

