//
//  NumpadView.swift
//  MavrikApp
//
//  Created by AK on 18.10.2020.
//

import UIKit

protocol KeyboardDelegate: class {
    func keyWasTapped(character: Int)
}

class NumpadView: UIInputView {
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: KeyboardDelegate?
    
    var intrinsicHeight: CGFloat = 408 {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    init() {
        super.init(frame: CGRect(), inputViewStyle: .keyboard)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: self.intrinsicHeight)
    }
    
    func configView(vc: UIViewController, doneSelector: Selector) {
        configCollectionView()
        self.backgroundColor = .white
        popupView.withBorder(color: UIColor.init(hex: Constants.Colors.lightGray), width: 1.2, radius: 25, maskToBounds: true)
        doneButton.addTarget(vc, action: doneSelector, for: .touchUpInside)
    }
    
    private func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "NumCellView", bundle: nil), forCellWithReuseIdentifier: "numCell")
    }
}

extension NumpadView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: NumCellView = collectionView.dequeueReusableCell(withReuseIdentifier: "numCell", for: indexPath) as! NumCellView
        switch indexPath.item {
        case 9:
            cell.numLabel.text = ""
            cell.deleteIcon.isHidden = true
        case 10:
            cell.numLabel.text = "0"
            cell.deleteIcon.isHidden = true
        case 11:
            cell.deleteIcon.isHidden = false
            cell.numLabel.text = ""
        default:
            cell.numLabel.text = String(indexPath.item + 1)
            cell.deleteIcon.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! NumCellView
        switch indexPath.item {
        case 9:
            print("")
        case 10:
            cell.backgroundColor = Constants.Colors.defaultBlue
            cell.numLabel.textColor = .white
            self.delegate?.keyWasTapped(character: 0)
        case 11:
            cell.backgroundColor = Constants.Colors.defaultBlue
            self.delegate?.keyWasTapped(character: 10)
        default:
            cell.backgroundColor = Constants.Colors.defaultBlue
            cell.numLabel.textColor = .white
            self.delegate?.keyWasTapped(character: indexPath.item + 1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! NumCellView
        cell.backgroundColor = .none
        cell.numLabel.textColor = Constants.Colors.defaultBlue
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 3) - 8, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
