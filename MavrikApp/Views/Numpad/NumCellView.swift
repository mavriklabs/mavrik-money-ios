//
//  NumCellView.swift
//  MavrikApp
//
//  Created by AK on 18.10.2020.
//

import UIKit

class NumCellView: UICollectionViewCell {

    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var deleteIcon: UIImageView!
    
}
