//
//  LoaderView.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import UIKit

class LoaderView: UIView {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var loaderIcon: UIImageView!
    @IBOutlet weak var loaderLabel: UILabel!
    @IBOutlet weak var loaderStack: UIStackView!
    @IBOutlet weak var okButtonView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var popupCenterYConstraint: NSLayoutConstraint!
    
    private var viewName = ""
    
    func setName(_ name: String) {
        viewName = name
    }
    
    func getName() -> String {
        return viewName
    }
    
    func configLoaderViewWithDone(withText text: String, done: Bool) {
        popupView.withRadius(15)
        loaderLabel.text = text
        okButtonView.isHidden = true
        cancelButton.isHidden = true
        lottieView.isHidden = true
    }
    
    func configLoaderView(withText text: String) {
        okButtonView.isHidden = true
        cancelButton.isHidden = true
        loaderLabel.isHidden = true
        lottieView.isHidden = true
        popupView.backgroundColor = .clear
        bgView.backgroundColor = .white
    }
    
    func configMessageView(withText text: String) {
        popupView.withRadius(15)
        lottieView.isHidden = true
        loaderIcon.isHidden = true
        cancelButton.isHidden = true
        loaderLabel.text = text
    }
    
    func configActionPrompt(with action: String, text: String) {
        popupView.withRadius(15)
        loaderIcon.isHidden = true
        lottieView.isHidden = true
        okButton.setTitle(action, for: .normal)
        loaderLabel.text = text
    }
    
    func configLogoutPrompt() {
        popupView.withRadius(15)
        loaderIcon.isHidden = true
        loaderLabel.isHidden = true
        lottieView.isHidden = true
        
        okButtonView.withBorder(color: Constants.Colors.defaultRed, width: 1, radius: 10, maskToBounds: true)
        okButton.setBackgroundImage(nil, for: .normal)
        okButton.setTitle("LOGOUT", for: .normal)
        okButton.setTitleColor(Constants.Colors.defaultRed, for: .normal)
        
        cancelButton.setTitleColor(Constants.Colors.defaultBlue, for: .normal)
        
        // update constraints to show popup at bottom instead of in the center
        popupView.translatesAutoresizingMaskIntoConstraints = false
        let bottomConstraint = NSLayoutConstraint(item: popupView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -50)
        let heightConstraint = popupView.heightAnchor.constraint(equalToConstant: 150)
        popupCenterYConstraint.isActive = false
        self.addConstraint(bottomConstraint)
        self.addConstraint(heightConstraint)
    }
}
