//
//  Step1Phone.swift
//  MavrikApp
//
//  Created by AK on 23.10.2020.
//

import UIKit

class Step1Phone: UIView {
    
    @IBOutlet weak var selectCountryCodeButton: UIButton!
    @IBOutlet weak var phoneCodeLabel: UILabel!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var flagIcon: UIImageView!
    
    
    func GetSlide() -> [Step1Phone] {
        let slide:Step1Phone = Bundle.main.loadNibNamed("Step1Phone", owner: self, options: nil)?.first as! Step1Phone
        return [slide]
    }
}
