//
//  Step1Verification.swift
//  MavrikApp
//
//  Created by AK on 23.10.2020.
//

import UIKit

class Step1Verification: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var resendButton: UIButton!
    
    func GetSlide() -> [Step1Verification] {
        let slide:Step1Verification = Bundle.main.loadNibNamed("Step1Verification", owner: self, options: nil)?.first as! Step1Verification
        return [slide]
    }
}
