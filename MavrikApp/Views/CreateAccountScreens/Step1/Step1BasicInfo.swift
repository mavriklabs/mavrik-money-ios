//
//  Step1BasicInfo.swift
//  MavrikApp
//
//  Created by AK on 23.10.2020.
//

import UIKit

class Step1BasicInfo: UIView {

    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!
    
    func GetSlide() -> [Step1BasicInfo] {
        let slide:Step1BasicInfo = Bundle.main.loadNibNamed("Step1BasicInfo", owner: self, options: nil)?.first as! Step1BasicInfo
        return [slide]
    }

}
