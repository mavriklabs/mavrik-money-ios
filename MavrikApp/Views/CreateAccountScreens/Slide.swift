//
//  Slide.swift
//  MavrikApp
//
//  Created by AK on 22.10.2020.
//

import UIKit

class Slide: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var step1Label: UILabel!
    @IBOutlet weak var step2Label: UILabel!
    @IBOutlet weak var step2Stack: UIStackView!
    @IBOutlet weak var step3Stack: UIStackView!
}
