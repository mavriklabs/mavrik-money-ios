//
//  HeaderView.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import UIKit

class HeaderView: UIView {
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    
    private var tableData: [AccountData] = []

    func configHeader() {
        profileButton.addTarget(self, action: #selector(profileDidTap), for: .touchUpInside)
        notificationButton.addTarget(self, action: #selector(notificationDidTap), for: .touchUpInside)
    }
    
    @objc private func profileDidTap() {
        self.parentViewController?.goToVC(withID: .ProfileVC, style: .pageSheet)
    }
    
    @objc private func notificationDidTap() {
        self.parentViewController?.goToVC(withID: .NotificationVC, style: .pageSheet)
    }
}
