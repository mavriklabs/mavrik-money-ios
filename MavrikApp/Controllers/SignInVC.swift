//
//  SignInVC.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit
import LocalAuthentication
import FirebaseCore
import FirebaseAuth
import GoogleSignIn
import AuthenticationServices
import Hero

class SignInVC: UIViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var emailFieldView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passPinFieldView: UIView!
    @IBOutlet weak var passPinField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var orLoginWithOtherLabel: UILabel!
    @IBOutlet weak var withEmailPin: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var withFaceTouchId: UIButton!
    
    @IBOutlet weak var loginWithEmailButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    @IBOutlet weak var appleButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    
    //MARK:- Variables
    let storage = LocalStorage()
    private var keyboardHeight: CGFloat = 0
    var loaderAnimation: LoaderAnimation!
    var socialLogin: SocialLogin!
    var currentNonce: String?
    
    //MARK:- Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loaderAnimation = LoaderAnimation(vc: self)
        self.socialLogin = SocialLogin(vc: self, loaderAnimation: self.loaderAnimation, isSignUpFlow: false)
        initUI()
        initTargets()
        initDelegates()
        initHideKeyboard()
    }
    
    private func initDelegates() {
        passPinField.delegate = self
        emailField.delegate = self
    }
    
    private func initUI() {
        emailFieldView.withRadius(10)
        passPinFieldView.withRadius(10)
        signUpButton.withRadius(10)
        signUpButton.withBorder(color: #colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1))
    }
    
    private func initTargets() {
        signUpButton.addTarget(self, action: #selector(signUpDidTap), for: .touchUpInside)
        loginWithEmailButton.addTarget(self, action: #selector(loginWithEmailDidTap), for: .touchUpInside)
        forgotPasswordButton.addTarget(self, action: #selector(forgotPasswordDidTap), for: .touchUpInside)
        googleButton.addTarget(self, action: #selector(googleDidTap), for: .touchUpInside)
        appleButton.addTarget(self, action: #selector(appleDidTap), for: .touchUpInside)
    }

    @objc private func googleDidTap() {
        // Configure the Google Sign In instance
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()!.options.clientID
        GIDSignIn.sharedInstance().delegate = self.socialLogin

        // Start the sign in flow!
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc private func appleDidTap() {
        socialLogin.startSignInWithAppleFlow()
    }
    
    @objc private func signUpDidTap() {
        self.goToVC(withID: .CreateNewAccountVC, style: .fullScreen)
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
    
    @objc private func loginWithEmailDidTap() {
        let email = emailField.text ?? ""
        let password = passPinField.text ?? ""
        let emailValid = isValidEmail(email: email)
        let passValid = isValidPassword(pass: password)
        if !emailValid {
            emailFieldView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: emailFieldView, vc: self, completion: {})
        }
        if !passValid {
            passPinFieldView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: passPinFieldView, vc: self, completion: {})
        }
        if !emailValid || !passValid {
            return
        }
        dismissKeyboard()
        // clear border red
        emailFieldView.withBorder(color: .clear)
        passPinFieldView.withBorder(color: .clear)
        
        self.loaderAnimation.showLoader(withText: "Authenticating")
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            if (error != nil) {
               print("Firebase sign in user with email \(email) error: \(error!.localizedDescription)")
               self.handleError(error!)
               return
            }
            // fetch and store firebase user profile data locally
            CustomUtils.fetchAndStoreFirebaseUserProfile() { _ in
                print("User signed in with email \(email) and password")
                let vcId = CustomUtils.getScreenToPresent()
                self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
            }
        }
    }
    
    @objc private func forgotPasswordDidTap() {
        self.goToVC(withID: .ForgotPasswordVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
    }
}

//MARK:- TextField Delegate Extension
extension SignInVC: UITextFieldDelegate {
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailField:
            passPinField.becomeFirstResponder()
        default:
            loginWithEmailDidTap()
        }
        return true
    }
    
    func initHideKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
