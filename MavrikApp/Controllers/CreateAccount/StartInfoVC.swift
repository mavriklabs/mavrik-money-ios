//
//  StartInfoVC.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit
import Hero


class StartInfoVC: UIViewController {
        
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    var slides:[Slide] = [];
    
    var isStep2 = false {
        didSet {
            self.configUI(step2: isStep2)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        isStep2 = false
        backButton.isHidden = false
        startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControll.numberOfPages = slides.count
        pageControll.currentPage = 0
        view.bringSubviewToFront(pageControll)
    }
    
    private func configUI(step2: Bool) {
        startButton.setTitle(!step2 ? "Get Started!" : "Got it!", for: .normal)
    }
    
    private func animateStep() {
    }
    
    @objc private func startDidTap() {
        switch pageControll.currentPage {
        case 0:
            scrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
        default:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.Step1PhoneVC.rawValue)
            vc.hero.isEnabled = true
            vc.modalPresentationStyle = .fullScreen
            vc.hero.modalAnimationType = HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right))
            self.present(vc, animated: true)
        }

    }
    
    @objc private func backDidTap() {
        switch pageControll.currentPage {
        case 0:
            //self.dismiss(animated: true, completion: nil)
            self.goToVC(withID: .CreateNewAccountVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
        default:
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }
    }

}


//MARK:- ScrollView Delegate Extension
extension StartInfoVC: UIScrollViewDelegate {
    func setupSlideScrollView(slides : [Slide]) {
        scrollView.contentSize = CGSize(width: self.view.frame.width * CGFloat(slides.count), height: 1)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.view.frame.width * CGFloat(i), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func createSlides() -> [Slide] {
        let slide1:Slide = Bundle.main.loadNibNamed("StartInfoSlide", owner: self, options: nil)?.first as! Slide
        slide1.descrLabel.text = "Create an account in just\n2 steps :"
        slide1.step3Stack.isHidden = true
        slide1.step1Label.text = "Share basic information"
        slide1.step2Label.text = "Submit documentation"
        
        let slide2:Slide = Bundle.main.loadNibNamed("StartInfoSlide", owner: self, options: nil)?.first as! Slide
        slide2.descrLabel.text = "These are the documents\nyou’ll need :"
        slide2.step3Stack.isHidden = true
        slide2.step2Stack.isHidden = true
        slide2.step1Label.text = "Address Proof"
        
        return [slide1, slide2]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.width)
        pageControll.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        
        let _: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        if pageIndex == 0 {
            isStep2 = false
        } else {
            isStep2 = true
        }
    }
}
