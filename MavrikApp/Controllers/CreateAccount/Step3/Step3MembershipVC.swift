//
//  Step3MembershipVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit

enum SelectedMembership {
    case none, premium, basic
}

class Step3MembershipVC: UIViewController {

    @IBOutlet weak var premiumView: UIView!
    @IBOutlet weak var premiumGradientView: UIView!
    @IBOutlet weak var basicView: UIView!
    @IBOutlet weak var basicGradientView: UIView!
    @IBOutlet weak var premiumButton: UIButton!
    @IBOutlet weak var basicButton: UIButton!
    
    private var selectedTarif: SelectedMembership = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        congigUI()
        addTargets()
    }
    
    private func congigUI() {
    //    premiumGradientView.withRadius(15)
        premiumGradientView.withRadius(15)
        premiumView.backgroundColor = .clear
        basicGradientView.withRadius(15)
        basicView.backgroundColor = .clear
        premiumButton.withRadius(10)
        premiumButton.withBorder(color: .white)
        basicButton.withRadius(10)
        basicButton.withBorder(color: .white)
        
        let gradientLayer: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = CGRect.zero
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            gradientLayer.colors = [ #colorLiteral(red: 0.968627451, green: 0.7921568627, blue: 0.6235294118, alpha: 1).cgColor, #colorLiteral(red: 0.9568627451, green: 0.7098039216, blue: 0.4705882353, alpha: 1).cgColor, #colorLiteral(red: 0.9764705882, green: 0.6901960784, blue: 0.4196078431, alpha: 1).cgColor, #colorLiteral(red: 0.9529411765, green: 0.6196078431, blue: 0.3019607843, alpha: 1).cgColor]
            return gradientLayer
        }()
        self.premiumGradientView.layer.addSublayer(gradientLayer)
        gradientLayer.frame = self.premiumGradientView.bounds
        gradientLayer.cornerRadius = 15
        gradientLayer.masksToBounds = true
        
        
        let gradientLayerBasic: CAGradientLayer = {
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = CGRect.zero
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            gradientLayer.colors = [ #colorLiteral(red: 0.431372549, green: 0.5450980392, blue: 0.9647058824, alpha: 1).cgColor, #colorLiteral(red: 0.3254901961, green: 0.4549019608, blue: 0.9294117647, alpha: 1).cgColor]
            return gradientLayer
        }()
        self.basicGradientView.layer.addSublayer(gradientLayerBasic)
        gradientLayerBasic.frame = self.basicGradientView.bounds
    }
    
    private func addTargets() {
        premiumButton.addTarget(self, action: #selector(premiumDidTap), for: .touchUpInside)
        basicButton.addTarget(self, action: #selector(basicDidTap), for: .touchUpInside)
    }
    
    @objc private func premiumDidTap() {
        basicButton.backgroundColor = .clear
        basicButton.setTitleColor(.white, for: .normal)
        premiumButton.backgroundColor = .white
        premiumButton.setTitleColor(#colorLiteral(red: 0.979952395, green: 0.495452106, blue: 0, alpha: 0.6535391178), for: .normal)
        selectedTarif = .premium
        goToNextVC()
    }
    
    @objc private func basicDidTap() {
        premiumButton.backgroundColor = .clear
        premiumButton.setTitleColor(.white, for: .normal)
        basicButton.backgroundColor = .white
        basicButton.setTitleColor(#colorLiteral(red: 0.979952395, green: 0.495452106, blue: 0, alpha: 0.6535391178), for: .normal)
        selectedTarif = .basic
        goToNextVC()
    }
    
    private func goToNextVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.AddPaymentVC.rawValue) as! AddPaymentVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }

}
