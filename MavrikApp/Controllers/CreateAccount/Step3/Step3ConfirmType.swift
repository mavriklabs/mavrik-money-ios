//
//  Step3ConfirmType.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import FirebaseAuth
import Hero
import LocalAuthentication

class Step3ConfirmType: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pinButton: UIButton!
    @IBOutlet weak var faceIDButton: UIButton!
    
    private let storage = LocalStorage()

    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        addTargets()
    }
    
    private func initUI() {
        backButton.isHidden = true
        pinButton.withRadius(10)
        pinButton.withBorder(color: #colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1))
        if getBiometryType() == "" {
            faceIDButton.isHidden = true
        } else {
            faceIDButton.withRadius(10)
            faceIDButton.withBorder(color: #colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1))
            faceIDButton.setTitle(getBiometryType(), for: .normal)
        }
    }
    
    private func addTargets() {
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        pinButton.addTarget(self, action: #selector(pinDidSelect), for: .touchUpInside)
        faceIDButton.addTarget(self, action: #selector(faceIdDidSelect), for: .touchUpInside)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func pinDidSelect() {
        self.goToVC(withID: .Step3EnterPinVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
    }
    
    @objc private func faceIdDidSelect() {
        // set 2fa
        authenticateWithBiometrics() { result, errorCode in
            let result = result ?? false
            if result {
                NotificationManager.registerForAPNS(callback: {_ in })
                
                // update mfa type and mfa set bool in local storage
                self.storage.setAuthKey(key: .mfaTypeFaceId, value: true)
                self.storage.setProfileKey(key: .multiFactorSet, value: true)
                
                // update mfaset bool in firebase
                let userId = Auth.auth().currentUser!.uid
                let data = [LocalProfileKeys.multiFactorSet.rawValue: true]
                CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
                
                let vcId = CustomUtils.getScreenToPresent()
                DispatchQueue.main.async {
                    self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
                }
            } else {
                print("Could not set biometric 2fa")
                if let code = errorCode {
                    if code == LAError.biometryNotAvailable.rawValue {
                        self.showBiometryDeniedAlert()
                    } else {
                        self.showAlert(withText: "\(self.getBiometryType()) not available")
                    }
                }
            }
        }
    }
}
