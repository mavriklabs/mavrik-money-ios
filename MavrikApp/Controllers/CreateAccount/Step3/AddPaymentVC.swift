//
//  Step3PaymentVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit

class AddPaymentVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!

    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var cardNumberView: UIView!
    @IBOutlet weak var cardNumberField: UITextField!
    
    @IBOutlet weak var cardholderNameView: UIView!
    @IBOutlet weak var cardholderNameField: UITextField!
    
    @IBOutlet weak var expiryDateView: UIView!
    @IBOutlet weak var expiryDateField: UITextField!
    
    @IBOutlet weak var cvvView: UIView!
    @IBOutlet weak var svvField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        configUI()
        hideKeyboard()
    }
    
    private func configUI() {
        cardNumberView.withRadius(10)
        cardholderNameView.withRadius(10)
        expiryDateView.withRadius(10)
        cvvView.withRadius(10)
        svvField.isSecureTextEntry = true
        cardNumberField.delegate = self
        cardholderNameField.delegate = self
        expiryDateField.delegate = self
    }
    
    private func addTargets() {
        startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    @objc private func startDidTap() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.Step3ConfirmType.rawValue) as! Step3ConfirmType
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddPaymentVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.returnKeyType = .done
    }
    
    func hideKeyboard() {
        let tap = UISwipeGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.direction = .down
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
