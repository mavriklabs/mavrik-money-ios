//
//  Step3EnterPinVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import Hero
import FirebaseAuth

class Step3EnterPinVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var pinField: UITextField!
    @IBOutlet weak var confirmPinView: UIView!
    @IBOutlet weak var confirmPinField: UITextField!
    
    private let storage = LocalStorage()
    private var loaderAnimation: LoaderAnimation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation.init(vc: self)
        addTargets()
        pinField.becomeFirstResponder()
        initHideKeyboard()
        configUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configUI() {
        confirmPinField.delegate = self
        pinField.delegate = self
        pinView.withRadius(10)
        confirmPinView.withRadius(10)
    }
    
    private func addTargets() {
        startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        pinField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        confirmPinField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc private func textFieldDidChange(_ currentField: UITextField) {
        currentField.font = Constants.Fonts.montSerratBold25
        if currentField.text == "" {
            currentField.font = Constants.Fonts.montSerratRegular14
        }
    }
    
    @objc private func startDidTap() {
        let pin = pinField.text ?? ""
        let confirmPin = confirmPinField.text ?? ""
        
        if pinField.text == "" {
            pinView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: pinView, vc: self, completion: {})
            return
        }
        if confirmPinField.text == "" {
            confirmPinView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: confirmPinView, vc: self, completion: {})
            return
        }
        // clear border red
        dismissKeyboard()
        pinView.withBorder(color: .clear)
        confirmPinView.withBorder(color: .clear)
        
        if pin.count < 4 {
            loaderAnimation.showMessage(withText: "PIN must have minimum 4 digits")
            return
        }
        if pin != confirmPin {
            loaderAnimation.showMessage(withText: "PINs don't match")
            return
        }
        
        // get hash of pin
        let hash = CustomUtils.getSHA256(for: pin)
        
        // store auth type and the actual pin in local storage
        self.storage.setAuthKey(key: .mfaTypePin, value: true)
        self.storage.setAuthKey(key: .pin, value: hash)
        storage.setProfileKey(key: .multiFactorSet, value: true)
        
        // update firebase
        let userId = Auth.auth().currentUser!.uid
        let data = [LocalProfileKeys.multiFactorSet.rawValue: true]
        CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
        
        // ask for push notification consent
        NotificationManager.registerForAPNS(callback: {_ in })
        
        let vcId = CustomUtils.getScreenToPresent()
        self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- TextField Delegate Extension
extension Step3EnterPinVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func initHideKeyboard() {
        let tap = UITapGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        let swipe = UISwipeGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
