//
//  CameraVC.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit
import AVFoundation
import Hero

class CameraVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var vcTitle: UILabel!
    @IBOutlet weak var vcDescr: UILabel!
    @IBOutlet weak var viewForCamera: UIView!
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var videoStreamView: UIView!
    
    var captureSesssion: AVCaptureSession!
    var cameraOutput: AVCapturePhotoOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    private var hasCameraAccess = false
    
    var vcTitleCurrent: String?
    
    var capturedImage: UIImage? {
        didSet {
            if capturedImage != nil {
                print("We have a picture")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.CheckPhotoVC.rawValue) as! CheckPhotoVC
                vc.image = capturedImage
                vc.vcTitleCurrent = self.vcTitle.text
                vc.hero.isEnabled = true
                vc.modalPresentationStyle = .fullScreen
                vc.hero.modalAnimationType = HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right))
                self.present(vc, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startSession()
        viewForCamera.withRadius(15)
        cameraButton.addTarget(self, action: #selector(cameraDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        backButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if vcTitleCurrent != nil {
            vcTitle.text = vcTitleCurrent
        }
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cameraDidTap() {
        checkCameraAccess()
        if hasCameraAccess {
            capturePhoto()
        }
    }
    
    func capturePhoto() {
        let settings = AVCapturePhotoSettings()
        settings.isHighResolutionPhotoEnabled = true
        settings.flashMode = .auto
        guard let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first else {
            print("Error getting preview pixel type")
            return
        }
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: viewForCamera.frame.width,
            kCVPixelBufferHeightKey as String: viewForCamera.frame.height
        ] as [String : Any]
        settings.previewPhotoFormat = previewFormat
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
}

//MARK:- Camera Delegate Extension
extension CameraVC: AVCapturePhotoCaptureDelegate {
    
    func getDevice() -> AVCaptureDevice? {
        if let device = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .back) {
            return device
        } else if let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) {
            return device
        } else {
            print("Error starting camera session")
            return nil
        }
    }
    
    func startSession() {
        // check if camera permission is granted
        checkCameraAccess()
        if !hasCameraAccess {
            print("Cannot start session as there is no camera access")
            return
        }
        if captureSesssion != nil && captureSesssion.isRunning {
            print("Capture session already running")
            return
        }
        captureSesssion = AVCaptureSession()
        captureSesssion.sessionPreset = AVCaptureSession.Preset.photo
        cameraOutput = AVCapturePhotoOutput()
        cameraOutput.isHighResolutionCaptureEnabled = true
        
        let device = getDevice()
        if let device = device {
            do {
                let input = try AVCaptureDeviceInput(device: device)
                if (captureSesssion.canAddInput(input)) {
                    captureSesssion.addInput(input)
                    if (captureSesssion.canAddOutput(cameraOutput)) {
                        captureSesssion.addOutput(cameraOutput)
                        previewLayer = AVCaptureVideoPreviewLayer(session: captureSesssion)
                        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                        previewLayer.frame = viewForCamera.bounds
                        videoStreamView.layer.addSublayer(previewLayer)
                        captureSesssion.startRunning()
                    } else {
                        print("Error: Capture session cannot add output")
                    }
                } else {
                    print("Error: Capture session cannot add input")
                }
            } catch {
                print("Error starting camera session \(error.localizedDescription)")
            }
        } else {
            print("Error starting camera session. No back camera present.")
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard error == nil else {
            print("Error capturing photo: \(error!.localizedDescription)")
            return
        }
        
        guard let imageData = photo.fileDataRepresentation() else {
            print("Error while generating image from photo capture data.")
            return
        }
        
        guard let image = UIImage(data: imageData) else {
            print("Unable to generate UIImage from image data.")
            return
        }
        
        self.capturedImage = image
    }
    
    // Check camera permission
    func checkCameraAccess() {
        print("Checking camera permission")
        let cameraPermissionStatus =  AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch cameraPermissionStatus {
        case .authorized:
            print("Already Authorized")
            hasCameraAccess =  true
        case .denied:
            showDeniedAlert()
        case .restricted:
            print("Restricted camera access")
        default:
            requestAccess()
        }
    }
    
    func requestAccess() {
        AVCaptureDevice.requestAccess(for: .video) { granted in
            if granted {
                print("User granted camera access")
                self.hasCameraAccess = true
                DispatchQueue.main.async {
                    self.startSession()
                }
            }
            else {
                self.showDeniedAlert()
            }
        }
    }
    
    func showDeniedAlert() {
        print("Denied camera access")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Camera access denied" , message: "Mavrik needs camera access to capture KYC documents. Please allow access in settings.",  preferredStyle: .alert)
            let deny = UIAlertAction(title: "Deny", style: .default)
            let settings = UIAlertAction(title: "Settings", style: .default) { action in
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                if let settingsUrl = settingsUrl, UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl)
                }
            }
            alert.addAction(deny)
            alert.addAction(settings)
            self.present(alert, animated: true)
        }
    }
}
