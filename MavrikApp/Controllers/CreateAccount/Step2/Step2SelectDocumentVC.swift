//
//  Step2SelectDocumentVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import Hero

class Step2SelectDocumentVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var driverLicenseButton: UIButton!
    @IBOutlet weak var govIdButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    private var selectedButtonTag = 0 {
        didSet {
            self.initUI()
            self.configNextButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        configNextButton()
        addTargets()
        
        // get kyc docs from API
        let country = LocalStorage().getProfileKey(key: .residenceCountry) ?? ""
        ApiManager.getRequiredKycDocs(country: country, account: "") { data in
            let docs = data?.docs
            LocalStorage().setProfileKey(key: .requiredKycDocs, value: docs)
        }
    }
    
    private func addTargets() {
        //startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        driverLicenseButton.addTarget(self, action: #selector(docTypeIsSelected(_:)), for: .touchUpInside)
        govIdButton.addTarget(self, action: #selector(docTypeIsSelected(_:)), for: .touchUpInside)
        otherButton.addTarget(self, action: #selector(docTypeIsSelected(_:)), for: .touchUpInside)
    }
    
    private func initUI() {
        backButton.isHidden = true
        startButton.isHidden = true
        otherButton.isHidden = true
        driverLicenseButton.tag = 11
        configButton(driverLicenseButton)
        govIdButton.tag = 12
        configButton(govIdButton)
        otherButton.tag = 13
        configButton(otherButton)
    }
    
    private func configButton(_ button: UIButton) {
        if button.tag == selectedButtonTag {
            button.setBackgroundImage(UIImage(named: "gradButton"), for: .normal)
            button.setTitleColor(.white, for: .normal)
        } else {
            button.setBackgroundImage(nil, for: .normal)
            button.withRadius(10)
            button.withBorder(color: #colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1))
            button.setTitleColor(#colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1), for: .normal)
        }
    }
    
    private func configNextButton() {
        startButton.withRadius(10)
        if selectedButtonTag == 0 {
            startButton.backgroundColor = .lightGray
            startButton.setBackgroundImage(nil, for: .normal)
        } else {
            startButton.setBackgroundImage(UIImage(named: "gradButton"), for: .normal)
        }
    }
    
    @objc private func docTypeIsSelected(_ button: UIButton) {
        selectedButtonTag = button.tag
        print("Address proof selected with tag: \(selectedButtonTag)")
        goToNextKycCaptureScreen()
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
