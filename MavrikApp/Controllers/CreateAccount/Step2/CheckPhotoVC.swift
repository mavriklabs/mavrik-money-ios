//
//  CheckPhotoVC.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit
import Hero

class CheckPhotoVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var vcTitle: UILabel!
    @IBOutlet weak var vcDescr: UILabel!
    @IBOutlet weak var viewForCamera: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    
    @IBOutlet weak var redoButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    var image: UIImage?
    var vcTitleCurrent : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        redoButton.addTarget(self, action: #selector(redoDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(redoDidTap), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextDidTap), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if image != nil {
            imagePreview.image = image!
            viewForCamera.withRadius(15)
        }
        if vcTitleCurrent != nil {
            vcTitle.text = vcTitleCurrent
        }
    }
    
    @objc private func redoDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func nextDidTap() {
        //save image
        saveImage(imageName: vcTitle.text!, image: image!)
        //go to next step
        goToNextKycCaptureScreen()
    }
}
