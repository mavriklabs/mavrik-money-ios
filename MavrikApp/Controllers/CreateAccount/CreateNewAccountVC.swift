//
//  Step3EmailAndPassVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import FirebaseAuth
import FirebaseCore
import GoogleSignIn
import AuthenticationServices
import CryptoKit
import Hero


class CreateNewAccountVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var welcomeLabel: UILabel!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var passField: UITextField!
    
    @IBOutlet weak var appleButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var fieldConstr: NSLayoutConstraint! // 30
    
    let storage = LocalStorage()
    var loaderAnimation: LoaderAnimation!
    var socialLogin: SocialLogin!
    
    var currentNonce: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loaderAnimation = LoaderAnimation(vc: self)
        self.socialLogin = SocialLogin(vc: self, loaderAnimation: self.loaderAnimation, isSignUpFlow: true)
        configUI()
        addTargets()
        hideKeyboard()
        subscribeToNotification(UIResponder.keyboardWillShowNotification, selector: #selector(keyboardWillShow))
        subscribeToNotification(UIResponder.keyboardWillHideNotification, selector: #selector(keyboardWillHide))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromAllNotifications()
    }
    
    func unsubscribeFromAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        fieldConstr.constant = -50
        welcomeLabel.isHidden = true
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        fieldConstr.constant = 30
        welcomeLabel.isHidden = false
    }
    
    private func configUI() {
        emailField.delegate = self
        passField.delegate = self
        emailView.withRadius(10)
        passView.withRadius(10)
    }
    
    private func addTargets() {
        startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        googleButton.addTarget(self, action: #selector(googleDidTap), for: .touchUpInside)
        appleButton.addTarget(self, action: #selector(appleDidTap), for: .touchUpInside)
    }
    
    @objc private func startDidTap() {
        signUp()
    }
    
    @objc private func backDidTap() {
        //self.dismiss(animated: true, completion: nil)
        dismissKeyboard()
        self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .down), dismissing: .slide(direction: .up)))
    }
    
    @objc private func googleDidTap() {
        // Configure the Google Sign In instance
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()!.options.clientID
        GIDSignIn.sharedInstance().delegate = self.socialLogin
        
        // Start the sign in flow!
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @objc private func appleDidTap() {
        socialLogin.startSignInWithAppleFlow()
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
    
    private func signUp() {
        let email = emailField.text ?? ""
        let password = passField.text ?? ""
        let emailValid = isValidEmail(email: email)
        let passValid = isValidPassword(pass: password)
        if !emailValid {
            emailView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: emailView, vc: self, completion: {})
        }
        if !passValid {
            passView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: passView, vc: self, completion: {})
        }
        if !emailValid || !passValid {
            return
        }
        dismissKeyboard()
        // clear border red
        emailView.withBorder(color: .clear)
        passView.withBorder(color: .clear)
        loaderAnimation.showLoader(withText: "Signing up")
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if (error != nil) {
                print("Firebase signup user with email password error: \(error!.localizedDescription)")
                self.handleError(error!)
                return
            }
            
            let user = Auth.auth().currentUser
            if let user = user {
                let email = user.email ?? ""
                if email == "" {
                    self.loaderAnimation.showMessage(withText: "Email is empty")
                    return
                }
                
                let photoURL = user.photoURL?.absoluteString ?? ""
                let name = user.displayName ?? ""
                
                var userInfo: [String: Any] = [:]
                if name != "" {
                    userInfo[LocalProfileKeys.name.rawValue] = name
                }
                userInfo["uid"] = user.uid
                userInfo[LocalProfileKeys.email.rawValue] = email
                userInfo["photoURL"] = photoURL
                userInfo["emailVerified"] = user.isEmailVerified
                userInfo[LocalProfileKeys.loginMethod.rawValue] = Constants.LoginMethods.emailPassword
                
                CustomUtils.registerUserInFirebaseAndDynamoDb(user: userInfo)
                //store locally
                self.storage.setProfileKey(key: .name, value: name)
                self.storage.setProfileKey(key: .email, value: email)
                self.storage.setProfileKey(key: .loginMethod, value: Constants.LoginMethods.emailPassword)
                
                print("User signed up with email/password with email \(email)")
                self.goToVC(withID: .StartInfoVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
            }
        }
    }
}


//MARK:- TextField Delegate Extension
extension CreateNewAccountVC: UITextFieldDelegate {
    
    func subscribeToNotification(_ notification: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: notification, object: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailField:
            self.passField.becomeFirstResponder()
        default:
            self.signUp()
        }
        return true
    }
    
    func hideKeyboard() {
        let tap = UITapGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        
        let swipe = UISwipeGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}
