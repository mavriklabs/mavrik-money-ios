//
//  Step1PhoneVC.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit
import Hero
import FirebaseAuth
import SafariServices

class Step1VC: UIViewController, KeyboardDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var startButtonConstraint: NSLayoutConstraint! // 30
    
    var slides: [UIView] = []
    //let keyboardView = NumpadView().loadNib() as! NumpadView
    
    private var currentField: UITextField?
    private var currentCountryCode: String?
    private var phoneNumber: String?
    private var phoneNumberFormatted: String?
    private var loaderAnimation: LoaderAnimation!
    private var isPhoneVerified = false
    
//    private var startButtonConstraintKeyboardHidden: NSLayoutConstraint!
//    private var startButtonConstraintKeyboardShown: NSLayoutConstraint!
    private var keyboardHeight: CGFloat = 250
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loaderAnimation = LoaderAnimation(vc: self)
        scrollView.delegate = self
        startButton.addTarget(self, action: #selector(startDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        hideKeyboard()
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func startDidTap() {
        switch pageControl.currentPage {
        case 0:
            self.getSMSCode()
        case 1:
            self.sendSMSCode()
        default:
            // update name, phone and country locally and in firebase
            print("Storing phone, name and country locally")
            let infoScreen = slides[2] as! Step1BasicInfo
            let name = infoScreen.textField.text
            let country = infoScreen.countryLabel.text
            LocalStorage().setProfileKey(key: .name, value: name)
            LocalStorage().setProfileKey(key: .residenceCountry, value: country)
            LocalStorage().setProfileKey(key: .phone, value: phoneNumberFormatted)
            
            var data: [String: String] = [:]
            data[LocalProfileKeys.name.rawValue] = name
            data[LocalProfileKeys.residenceCountry.rawValue] = country
            data[LocalProfileKeys.phone.rawValue] = phoneNumberFormatted
            let userId = Auth.auth().currentUser!.uid
            // write to firebase
            print("Storing phone, name and country in firebase")
            CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.Step2SelectDocumentVC.rawValue) as! Step2SelectDocumentVC
            vc.hero.isEnabled = true
            vc.modalPresentationStyle = .fullScreen
            vc.hero.modalAnimationType = HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right))
            self.present(vc, animated: true)
        }
    }
    
    @objc private func backDidTap() {
        switch pageControl.currentPage {
        case 0:
            dismissKeyboard()
            self.dismiss(animated: true, completion: nil)
        case 1:
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        default:
            scrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
        }
    }
    
    @objc private func selectCountryDialCode() {
        print("Selecting country dial code")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.SearchCountryCodeVC.rawValue) as! SearchCountryCodeVC
        vc.country = { countryModel in
            DispatchQueue.main.async {
                if let label = self.view.viewWithTag(334) as? UILabel {
                    label.text = countryModel.dial_code
                    // store country locally
                    LocalStorage().setProfileKey(key: .residenceCountry, value: countryModel.name)
                    LocalStorage().setProfileKey(key: .residenceCountryCode, value: countryModel.code)
                }
                if let img = self.view.viewWithTag(335) as? UIImageView {
                    img.image = UIImage(named: countryModel.code)
                }
            }
        }
        self.present(vc, animated: true)
    }
    
    @objc private func selectCountry() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.SearchCountryCodeVC.rawValue) as! SearchCountryCodeVC
        vc.country = { countryModel in
            DispatchQueue.main.async {
                if let img = self.view.viewWithTag(112) as? UIImageView {
                    img.image = UIImage(named: countryModel.code)
                }
                if let country = self.view.viewWithTag(113) as? UILabel {
                    country.text = countryModel.name
                    country.textColor = .black
                }
            }
        }
        self.present(vc, animated: true)
    }
    
    @objc private func resendDidTap() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func keyWasTapped(character: Int) {
        if character == 10 {
            if currentField?.text! != "" {
                var text = currentField?.text! ?? ""
                text.removeLast()
                currentField?.text = text
            }
            return
        }
        currentField?.text = (currentField?.text! ?? "") + String(character)
    }
    
    @objc func numDoneDidTap() {
        currentField?.resignFirstResponder()
        if currentField?.tag == 333 {
            self.getSMSCode()
        } else if currentField?.tag == 444 {
            self.sendSMSCode()
        }
    }
    
    private func getPhone() -> String {
        var retPhone = ""
        if let codeLabel = view.viewWithTag(334) as? UILabel {
            let code = codeLabel.text!
            if currentField?.text! == "" {
                self.loaderAnimation.showMessage(withText: "Please enter a phone number.")
            } else if currentField != nil {
                let phoneFormatted = code + " " + currentField!.text!
                let phone = code + currentField!.text!
                self.phoneNumberFormatted = phoneFormatted
                self.phoneNumber = phone
                retPhone = phone
            }
        }
        return retPhone
    }
    
    private func getSMSCode() {
        self.loaderAnimation.showLoader(withText: "Sending SMS")
        let phone = getPhone()
        if phone == "" {
            print("Phone number empty")
            return
        }
        print("Getting sms code for phone: \(phone)")
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { verificationId, error in
            if let error = error {
                print("Error occured in verifying phone number: \(error.localizedDescription)")
                self.handleError(error)
            } else {
                print("Sent sms code to: \(phone)")
                // Store the verificationID for later use
                LocalStorage().setAuthKey(key: .phoneVerificationId, value: verificationId)
                // move to next screen
                self.loaderAnimation.removeLoader(withAnimation: .popOut)
                self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
            }
            
        }
    }
    
    private func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
    
    private func sendSMSCode() {
        self.loaderAnimation.showLoader(withText: "Verifying SMS")
        let verificationCode = currentField?.text
        if let verificationCode = verificationCode {
            let verificationId = LocalStorage().getAuthKey(key: .phoneVerificationId) ?? ""
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: verificationCode)
            // link to current user
            Auth.auth().currentUser?.link(with: credential) { result, error in
                if let error = error {
                    if error._code == AuthErrorCode.providerAlreadyLinked.rawValue {
                        print("Phone number already linked to this account")
                        self.loaderAnimation.removeLoader(withAnimation: .popOut)
                        self.isPhoneVerified = true
                        self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width * 2, y: 0), animated: true)
                    } else {
                        print("Phone verification could not be completed: \(error.localizedDescription)")
                        self.handleError(error)
                    }
                } else {
                    print("Phone number successfully verified")
                    self.loaderAnimation.removeLoader(withAnimation: .popOut)
                    self.isPhoneVerified = true
                    self.scrollView.setContentOffset(CGPoint(x: self.view.frame.width * 2, y: 0), animated: true)
                }
            }
        }
    }
    
    @objc func verificationCodeIsFilled(notification: Notification) {
        if currentField?.tag == 444 && currentField?.text?.count == 6 {
            self.sendSMSCode()
        }
    }
    
    private func enableNextButton() {
        let image = UIImage(named: Constants.Images.gradButton)
        startButton.setTitle("NEXT", for: .normal)
        startButton.titleLabel?.font = Constants.Fonts.montSerratMedium14
        startButton.isEnabled = true
        startButton.setBackgroundImage(image, for: .normal)
        startButton.layer.borderWidth = 0
        startButton.layer.cornerRadius = 0
        startButton.setTitleColor(.white, for: .normal)
    }
    
    private func disableNextButton() {
        startButton.setTitle("PHONE NOT VERIFIED", for: .normal)
        startButton.titleLabel?.font = Constants.Fonts.montSerratRegular14
        startButton.setTitleColor(.lightGray, for: .normal)
        startButton.isEnabled = false
        startButton.setBackgroundImage(nil, for: .normal)
        startButton.layer.cornerRadius = 10
        startButton.layer.borderWidth = 1
        startButton.layer.borderColor = UIColor.lightGray.cgColor
    }
}

//MARK:- TextField Delegate Extension
extension Step1VC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 333 || textField.tag == 444 {
            currentField = textField
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            if pageControl.currentPage != 2 {
                animateConstraint(for: keyboardFrame.cgRectValue.height)
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        animateConstraint(for: 30)
    }
    
    private func animateConstraint(for change: CGFloat) {
        self.startButtonConstraint.constant = change
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideKeyboard() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        pageControl.isHidden = false
        self.view.endEditing(true)
    }
}


//MARK:- ScrollView Delegate Extension
extension Step1VC: UIScrollViewDelegate {
    func setupSlideScrollView(slides : [UIView]) {
        scrollView.contentSize = CGSize(width: self.view.frame.width * CGFloat(slides.count), height: 1)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: self.view.frame.width * CGFloat(i), y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func createSlides() -> [UIView] {
        let slide1:Step1Phone = Bundle.main.loadNibNamed("Step1Phone", owner: self, options: nil)?.first as! Step1Phone
        slide1.fieldView.withRadius(10)
        slide1.selectCountryCodeButton.addTarget(self, action: #selector(selectCountryDialCode), for: .touchUpInside)
        slide1.phoneField.delegate = self
        slide1.phoneField.becomeFirstResponder()
        slide1.phoneField.tag = 333
        slide1.phoneCodeLabel.tag = 334
        slide1.flagIcon.tag = 335
        
        let slide2:Step1Verification = Bundle.main.loadNibNamed("Step1Verification", owner: self, options: nil)?.first as! Step1Verification
        slide2.fieldView.withRadius(10)
        slide2.textField.tag = 444
        slide2.resendButton.addTarget(self, action: #selector(resendDidTap), for: .touchUpInside)
        slide2.textField.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.verificationCodeIsFilled), name: UITextField.textDidChangeNotification, object: slide2.textField)
        
        let slide3:Step1BasicInfo = Bundle.main.loadNibNamed("Step1BasicInfo", owner: self, options: nil)?.first as! Step1BasicInfo
        slide3.fieldView.withRadius(10)
        slide3.textField.delegate = self
        slide3.textField.tag = 111
        slide3.countryButton.addTarget(self, action: #selector(selectCountry), for: .touchUpInside)
        slide3.countryFlag.tag = 112
        slide3.countryLabel.tag = 113
        
        return [slide1, slide2, slide3]
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/scrollView.frame.width)
        if pageControl.currentPage != Int(pageIndex) {
            self.dismissKeyboard()
        }
        pageControl.currentPage = Int(pageIndex)
        // phone screen
        if pageControl.currentPage == 0 {
            backButton.isHidden = true
            enableNextButton()
            let phoneScreen = slides[0] as! Step1Phone
            phoneScreen.phoneField.becomeFirstResponder()
        }
        // verification screen
        if pageControl.currentPage == 1 {
            backButton.isHidden = false
            enableNextButton()
            let veriScreen = slides[1] as! Step1Verification
            let phone = phoneNumberFormatted ?? ""
            veriScreen.label.text = "We’ve sent a verification code\nto " + phone
            veriScreen.textField.becomeFirstResponder()
            veriScreen.textField.text = nil
        }
        // basic info screen
        if pageControl.currentPage == 2 {
            backButton.isHidden = false
            if !self.isPhoneVerified {
                disableNextButton()
            }
            let infoScreen = slides[2] as! Step1BasicInfo
            let name = LocalStorage().getProfileKey(key: .name) ?? ""
            let country = LocalStorage().getProfileKey(key: .residenceCountry) ?? ""
            let countryCode = LocalStorage().getProfileKey(key: .residenceCountryCode) ?? ""
            if name != "" {
                infoScreen.textField.text = name
            } else {
                infoScreen.textField.becomeFirstResponder()
            }
            if country != "" {
                infoScreen.countryLabel.text = country
                infoScreen.countryLabel.textColor = .black
            }
            if countryCode != "" {
                infoScreen.countryFlag.image = UIImage(named: countryCode)
            }
        }
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let _: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let _: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
    }
}
