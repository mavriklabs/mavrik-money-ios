//
//  SearchCountryCodeVC.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit

struct CountryCodeModel {
    let icon: String
    let name: String
    let code: String
}

class SearchCountryCodeVC: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    private var tableValues: [CountryCode] = []
    private var filteredTableValues: [CountryCode] = []
    
    var country: ((CountryCode) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        addValues()
        closeButton.addTarget(self, action: #selector(closeDidTap), for: .touchUpInside)

        // SearchBar text font
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        //textFieldInsideUISearchBar?.textColor = UIColor.red
        textFieldInsideUISearchBar?.font = UIFont.init(name: ("Montserrat-Medium"), size: 17.0)

        // SearchBar placeholder font
        let labelInsideUISearchBar = textFieldInsideUISearchBar!.value(forKey: "placeholderLabel") as? UILabel
        //labelInsideUISearchBar?.textColor = UIColor.red
        labelInsideUISearchBar?.font = UIFont.init(name: ("Montserrat-Medium"), size: 17.0)
    }
    
    private func addValues() {
        CountryCodeParser.getData { (array) in
            self.tableValues = array
            self.removeValues()
            self.tableView.reloadData()
        }
    }
    
    private func removeValues() {
        filteredTableValues = tableValues
        tableView.reloadData()
    }
    
    @objc private func closeDidTap() {
        self.dismiss(animated: true, completion: nil)
    }

}


//MARK:- TableView Extension
extension SearchCountryCodeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filteredTableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CountryCodeCell.cellId, for: indexPath) as! CountryCodeCell
        cell.configCell(filteredTableValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        CountryCodeCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.country?(filteredTableValues[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK:- Search Delegate Extension
extension SearchCountryCodeVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String) {
        if searchText == "" {
            filteredTableValues = tableValues
        } else {
            filteredTableValues = tableValues.filter({ (value) -> Bool in
                value.name.contains(searchText)
            })
        }
      tableView.reloadData()
    }
}

extension SearchCountryCodeVC: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchBar.text!)
    }
}
