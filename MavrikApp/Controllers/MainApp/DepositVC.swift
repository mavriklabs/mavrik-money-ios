//
//  DepositVC.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit


class DepositVC: UIViewController {
    
    //MARK:- Outlet's
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segment0ButtonView: UIView!
    @IBOutlet weak var segment0Button: UIButton!
    @IBOutlet weak var segment0Label: UILabel!
    
    @IBOutlet weak var segment1ButtonView: UIView!
    @IBOutlet weak var segment1Button: UIButton!
    @IBOutlet weak var segment1Label: UILabel!
    
    @IBOutlet weak var section1Label: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var section2Label: UILabel!
    
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var walletQRCode: UIImageView!
    
    @IBOutlet weak var withdrawView: UIView!
    @IBOutlet weak var withdrawTextFieldView: UIView!
    @IBOutlet weak var withdrawTextField: UITextField!
    @IBOutlet weak var withdrawSelectTokenView: UIView!
    @IBOutlet weak var withdrawNextButton: UIButton!
    @IBOutlet weak var withdrawBottomConstr: NSLayoutConstraint!
    
    
    //MARK:- Variables
    static var goToCurrentVC = false
    
    private var arrayData: [LinkedPayment] = []
    private var keyboardHeight: CGFloat = 0
    private var openedCells = Set<IndexPath>()
    private var tappedCell: Int?
    private var selectedSegment: Int = 0 {
        didSet {
            switch selectedSegment {
            case 0:
                withdrawView.isHidden = true
                walletView.isHidden = false
                segment0Button.setBackgroundImage(UIImage(named: "gradButton") as UIImage?, for: .normal)
                segment0Button.setTitleColor(.white, for: .normal)
                
                segment1ButtonView.backgroundColor = .clear
                segment1ButtonView.withBorder(color: Constants.Colors.defaultBlue)
                segment1Button.setBackgroundImage(nil, for: .normal)
                segment1Button.setTitle("Withdraw", for: .normal)
                segment1Button.setTitleColor(Constants.Colors.defaultBlue, for: .normal)
                
                section1Label.text = "From Bank Account"
                section2Label.isHidden = false
                section2Label.text = "From Crypto"
            case 1:
                withdrawView.isHidden = true
                walletView.isHidden = true
                segment1Button.setBackgroundImage(UIImage(named: "gradButton") as UIImage?, for: .normal)
                segment1Button.setTitleColor(.white, for: .normal)
                
                segment0ButtonView.backgroundColor = .clear
                segment0ButtonView.withBorder(color: Constants.Colors.defaultBlue)
                segment0Button.setBackgroundImage(nil, for: .normal)
                segment0Button.setTitle("Deposit", for: .normal)
                segment0Button.setTitleColor(Constants.Colors.defaultBlue, for: .normal)
                
                section1Label.text = "To Bank Account"
                section2Label.isHidden = true
            default:
                print("error")
            }
        }
    }
    
    
    //MARK:- Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        selectedSegment = 0
        initTable()
        initTargets()
        //addMenuToVC(fromTab: 2)
        observeToKeyboard()
        withdrawTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DepositVC.goToCurrentVC = false
        loadQRCode()
        getPaymentData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromAllNotifications()
    }
    
    func unsubscribeFromAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configUI() {
        tableHeightConstraint.constant = 78 * 3
        tableView.withRadius(10)
        walletView.withRadius(10)
        withdrawTextFieldView.withRadius(10)
        withdrawSelectTokenView.withRadius(10)
        segment0ButtonView.withRadius(10)
        segment1ButtonView.withRadius(10)
    }
    
    private func observeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        if keyboardHeight == 0 {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                keyboardHeight = keyboardSize.height
            }
        }
        withdrawBottomConstr.constant = keyboardHeight
        scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y + keyboardHeight), animated: true)
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        withdrawBottomConstr.constant = 8
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func initTargets() {
        segment0Button.tag = 0
        segment0Button.addTarget(self, action: #selector(segmentDidChange(_:)), for: .touchUpInside)
        segment1Button.tag = 1
        segment1Button.addTarget(self, action: #selector(segmentDidChange(_:)), for: .touchUpInside)
        withdrawNextButton.addTarget(self, action: #selector(nextDidTap), for: .touchUpInside)
    }
    
    @objc private func segmentDidChange(_ button: UIButton) {
        selectedSegment = button.tag
    }
    
    @objc private func nextDidTap() {
        self.performSegue(withIdentifier: "goToAddDepositVC", sender: nil)
    }
    
    private func loadQRCode() {
        ApiManager.getQRCode(forAddress: "abc", callback: { data in
            if let data = data {
                DispatchQueue.main.async {
                    self.walletQRCode.image = UIImage(data: data)
                }
            }
        })
    }
    
    private func getPaymentData() {
        ApiManager.getLinkedPayments(account: "abc") { (array) in
            self.arrayData = array
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "goToAddDepositVC" else { return }
        guard let destination = segue.destination as? AddDepositVC else { return }
        destination.paymentDetail = arrayData[tappedCell ?? 0]
        destination.operationType = (selectedSegment == 0) ? .deposit : .withdraw
    }
}


//MARK:- TableView Extension
extension DepositVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == arrayData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addBankCell")!
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDetailCell.cellID, for: indexPath) as! PaymentDetailCell
        cell.selectionStyle = .none
        cell.configCell(arrayData[indexPath.row])
        if openedCells.contains(indexPath) { cell.rotateIndicator() }
        if tappedCell == indexPath.row { cell.buttonAnimation(vc: self) }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == arrayData.count {
            return PaymentDetailCell.closedHeight
        }
        return openedCells.contains(indexPath) ? PaymentDetailCell.openHeight : PaymentDetailCell.closedHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tappedCell = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 3 {
            return
        }
        self.performSegue(withIdentifier: "goToAddDepositVC", sender: nil)
        tableView.reloadData()
    }
}


//MARK:- TextField Delegate Extension
extension DepositVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.returnKeyType = .done
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func hideKeyboard() {
        let tap = UISwipeGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.direction = .down
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
