//
//  ProfileVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import FirebaseAuth
import Kingfisher

class ProfileVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userAddressLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstr: NSLayoutConstraint!
    
    var loaderAnimation: LoaderAnimation!
    
    private var tableValues: [ProfileCellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation(vc: self)
        initTable()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        loadUserData()
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initTable() {
        tableValues = [ProfileCellModel(iconName: "customerService", title: Constants.ProfileSettings.customerService,
                                                                        descr: "Talk to us"),
                                                       ProfileCellModel(iconName: "notificationSettings", title: Constants.ProfileSettings.notifications,
                                                                        descr: "Push notification preferences"),
                                                       ProfileCellModel(iconName: "securitySettings", title: Constants.ProfileSettings.security,
                                                                        descr: "PIN & \(getBiometryType())"),
                                                       ProfileCellModel(iconName: "terms_logo", title: Constants.ProfileSettings.legal,
                                                                        descr: "Terms & Conditions and Policies"),
                                                       ProfileCellModel(iconName: "logoutIcon", title: Constants.ProfileSettings.logout, descr: "")]
        
        tableView.isScrollEnabled = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        reloadTable()
    }
    
    private func reloadTable() {
        tableHeightConstr.constant = CGFloat(tableValues.count) * ProfileCell.height
        self.tableView.reloadData()
    }
    
    private func loadUserData() {
        setUserProfileimage()
        self.userEmailLabel.text = LocalStorage().getProfileKey(key: .email)
        self.userNameLabel.text = LocalStorage().getProfileKey(key: .name)
        self.userPhoneLabel.text = LocalStorage().getProfileKey(key: .phone)
        self.userAddressLabel.text = LocalStorage().getProfileKey(key: .residenceCountry)
    }
    
    private func setUserProfileimage() {
        let photoUrl = LocalStorage().getProfileKey(key: .photoURL) ?? ""
        if photoUrl != "" {
            let url = URL(string: photoUrl)
            let processor = DownsamplingImageProcessor(size: userProfileImage.bounds.size) |> RoundCornerImageProcessor(cornerRadius: 20)
            userProfileImage.kf.indicatorType = .activity
            userProfileImage.kf.setImage(
                with: url,
                placeholder: UIImage(named: Constants.Images.profileIcon),
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ],
                progressBlock: nil,
                completionHandler: { result in
                    switch result {
                    case .success(let value):
                        print("Profile image set: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Setting profile image failed: \(error.localizedDescription)")
                    }
                })
        }
    }
    
    func logout() {
        do {
            // remove user profile from local storage
            LocalStorage().removeLocalProfile()
            try Auth.auth().signOut()
            self.goToVC(withID: .SignInVC, style: .fullScreen)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            loaderAnimation.showMessage(withText: signOutError.localizedDescription)
        }
    }
}


//MARK:- TableView Extension
extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.cellID, for: indexPath) as! ProfileCell
        cell.configCell(tableValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProfileCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cellTitle = tableValues[indexPath.row].title
        switch cellTitle {
        case Constants.ProfileSettings.customerService:
            self.goToVC(withID: .CustomerServicesVC, style: .formSheet)
        case Constants.ProfileSettings.membership:
            self.goToVC(withID: .MembershipSettingsVC, style: .formSheet)
        case Constants.ProfileSettings.notifications:
            self.goToVC(withID: .NotificationSettingsVC, style: .formSheet)
        case Constants.ProfileSettings.security:
            self.goToVC(withID: .SecuritySettingsVCNav, style: .formSheet)
        case Constants.ProfileSettings.legal:
            self.goToVC(withID: .TermsVC, style: .formSheet)
        case Constants.ProfileSettings.logout:
            // show prompt
            loaderAnimation.showLogoutPrompt()
        default:
            print("")
        }
    }
}
