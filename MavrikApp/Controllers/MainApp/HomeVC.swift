//
//  HomeVC.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import UIKit
import Hero

class HomeVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var tableData: [AccountData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initHeader()
        initTable()
        loadData()
    }
    
    private func initHeader() {
        // clear bg
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        let headerView = HeaderView().loadNib() as! HeaderView
        headerView.frame = self.view.frame
        headerView.configHeader()
        navigationController?.navigationBar.addSubview(headerView)
    }
    
    private func initTable() {
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    private func loadData() {
        ApiManager.getHomeScreenData { (object) in
            if let data = object {
                print("data: \(data)")
                self.tableData = data.accounts
                self.tableView.reloadData()
            }
        }
    }
}


//MARK:- TableView Extension
extension HomeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.textCellId, for: indexPath) as! TextCell
        cell.configCell(tableData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TextCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let _ = tableView.cellForRow(at: indexPath) as! TextCell
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.CryptoAccountVC.rawValue) // as! CryptoAccountVC
        self.present(vc, animated: true)
    }
    
}
