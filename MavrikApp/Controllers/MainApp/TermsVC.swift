//
//  TermsVC.swift
//  MavrikApp
//
//  Created by AK on 30.10.2020.
//

import UIKit

class TermsVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    var loaderAnimation: LoaderAnimation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        loaderAnimation = LoaderAnimation(vc: self)
        loaderAnimation.showLoader()
        print("Fetching terms and conditions")
        ApiManager.getTerms() { data in
            self.loaderAnimation.removeLoader(withAnimation: .buttonTap)
            if let data = data {
                let str = String(decoding: data, as: UTF8.self)
                self.textView.attributedText = str.htmlAttributedString()
            } else {
                print("No terms and conditions found")
            }
        }
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }

}
