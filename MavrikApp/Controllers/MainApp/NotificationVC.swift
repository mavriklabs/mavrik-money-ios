//
//  NotificationVC.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit
import FirebaseAuth

class NotificationVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    private var tableValues: [NotificationModel] = []
    private var updatedNotifs: [String: Any] = [:]
    
    var loaderAnimation: LoaderAnimation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        loaderAnimation = LoaderAnimation(vc: self)
        initTable()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // store notification on server
        if updatedNotifs.count > 0 {
            let user = Auth.auth().currentUser?.uid
            if let user = user {
                ApiManager.storeNotifications(user: user, params: updatedNotifs) { status in
                    print("Stored notifications in server with status: \(status)")
                }
            } else {
                print("Cannot store notifications as user is nil")
            }
        }
        super.viewWillDisappear(animated)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func goToVerifyEmailVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VCIDs.VerifyEmailVC.rawValue) as! VerifyEmailVC
        vc.modalPresentationStyle = .pageSheet
        vc.hideBackButton()
        self.present(vc, animated: true)
    }
    
    private func initTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        loadData()
    }
    
    private func loadData() {
        let user = Auth.auth().currentUser?.uid
        if let user = user {
            loaderAnimation.showLoader()
            ApiManager.getNotifications(user: user) { data in
                self.tableValues = data
                self.loaderAnimation.removeLoader(withAnimation: .buttonTap)
                self.tableView.reloadData()
            }
        } else {
            print("Cannot get notifications as user is nil")
        }
    }
    
    private func performCellAction(_ notification: NotificationModel, _ indexPath: IndexPath) {
        switch notification.action {
        case Constants.NotificationActions.verifyEmail:
            print("Verifying email")
            toggleNotificationRead(notification, indexPath)
            goToVerifyEmailVC()
        default:
            print("No action found")
            toggleNotificationRead(notification, indexPath)
        }
    }
    
    private func toggleNotificationRead(_ notification: NotificationModel, _ indexPath: IndexPath) {
        var notif = notification
        let read = notif.read ?? false
        notif.read = !read
        tableValues[indexPath.row] = notif
        let id = notif.id ?? ""
        updatedNotifs[id] = notif.read
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}


//MARK:- TableView Extension
extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationCell.cellID, for: indexPath) as! NotificationCell
        cell.configCell(tableValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let notification = tableValues[indexPath.row]
        performCellAction(notification, indexPath)
    }
}
