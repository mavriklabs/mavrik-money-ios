//
//  PaymentDetailVC.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit

class PaymentDetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var backButton: UIButton!
    
    private var openedCells = Set<IndexPath>()
    private var tappedCell: Int?
    private var arrayData: [LinkedPayment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTable()
        //addMenuToVC(fromTab: 4)
        //backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        getData()
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc private func deleteDidTap(_ button: UIButton) {
        self.performSegue(withIdentifier: "goToDeleteBankAccountVC", sender: nil)
    //    self.goToVC(withID: .DeleteBankAccountVC, style: .formSheet)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func getData() {
        MainDataApiManager.getLinkedPayments(account: "abc") { (array) in
            self.arrayData = array
            self.tableView.reloadData()
        }
    }
}


//MARK:- TableView Extension
extension PaymentDetailVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == arrayData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addBankCell")!
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDetalCell.cellID, for: indexPath) as! PaymentDetalCell
        cell.selectionStyle = .none
        cell.configCell(arrayData[indexPath.row])
        cell.deleteButton.addTarget(self, action: #selector(deleteDidTap(_:)), for: .touchUpInside)
        if openedCells.contains(indexPath) { cell.rotateIndicator() }
        if tappedCell == indexPath.row { cell.buttonAnimation(vc: self) }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == arrayData.count {
            return PaymentDetalCell.closedHeight
        }
        return openedCells.contains(indexPath) ? PaymentDetalCell.openHeight : PaymentDetalCell.closedHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == arrayData.count {
            return
        }
        tappedCell = indexPath.row
        if !openedCells.contains(indexPath) {
            openedCells.insert(indexPath)
        } else {
            openedCells.remove(indexPath)
        }
        tableView.reloadData()
    }
}
