//
//  CryptoAccountVC.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit

class CryptoAccountVC: UIViewController {

    //@IBOutlet weak var menuButton: UIButton!
    //@IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var goToCryptobalance: UIButton!
    @IBOutlet weak var cryptobalanceButton: UIButton!
    @IBOutlet weak var earnedLabel: UILabel!
    @IBOutlet weak var annuallyLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton0: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var depositButtonView: UIView!
    @IBOutlet weak var depositButton: UIButton!
    
    private var accountData: HomeAccountModel?
        
    private var tableValues: [CryptobalanceModel] = []
    private var animationIsShow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //addMenuToVC(fromTab: 1)
        configUI()
        addButtonActions()
        initTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAccountData()
        self.getCryptoData()
    }
    
    private func configUI() {
        depositButtonView.withRadius(10)
        depositButtonView.backgroundColor = #colorLiteral(red: 0.5224282146, green: 0.5548258424, blue: 0.9729731679, alpha: 1)
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func addButtonActions() {
        //menuButton.addTarget(self, action: #selector(menuButtonDidTap), for: .touchUpInside)
        //profileButton.addTarget(self, action: #selector(profileDidTap), for: .touchUpInside)
        goToCryptobalance.addTarget(self, action: #selector(goToCrypto), for: .touchUpInside)
        cryptobalanceButton.addTarget(self, action: #selector(goToCrypto), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        backButton0.addTarget(self, action: #selector(back0DidTap), for: .touchUpInside)
        depositButton.addTarget(self, action: #selector(depositDidTap), for: .touchUpInside)
    }
    
    @objc private func back0DidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func backDidTap() {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @objc private func depositDidTap() {
        self.goToVC(withID: .DepositVCNav, style: .automatic)
    }
    
//    @objc private func menuButtonDidTap() {
//        self.showMenu(selector: #selector(menuDidTap(_:)), pageIndex: 1)
//    }
    
//    @objc private func profileDidTap() {
//        self.goToVC(withID: .ProfileVC, style: .automatic)
//    }
    
    @objc private func goToCrypto() {
        scrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
    }
    
    
    private func getAccountData() {
        ApiManager.getHomeAccountBalance(account: "abc") { (object) in
            self.accountData = object
            if let data = object {
                DispatchQueue.main.async {
                    self.cryptobalanceButton.setTitle("$ \(data.balance ?? 0)", for: .normal)
                    self.earnedLabel.text = "$ \(data.earned ?? 0)"
                    self.annuallyLabel.text = "\(data.yield ?? "0%") Annually"
                }
            }
        }
    }
    
    private func getCryptoData() {
        animationIsShow = false
        ApiManager.getHomeAccountDetal(account: "abc") { (array) in
            self.tableValues = array
            self.tableView.reloadData()
        }
    }
    
}


//MARK:- TableView Extension
extension CryptoAccountVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CryptoBalanceCell.cellID, for: indexPath) as! CryptoBalanceCell
        cell.selectionStyle = .none
        cell.configCell(tableValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CryptoBalanceCell.height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !animationIsShow {
            Animations.cellAnimation(type: .alpha, cell: cell, indexPath: indexPath, completion: {})
        }
        if indexPath.row == tableValues.count - 1 {
            animationIsShow = true
        }
    }
}

