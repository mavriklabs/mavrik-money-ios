//
//  BanksVC.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit

class BanksVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var openedCells = Set<IndexPath>()
    private var tappedCell: Int?
    private var arrayData: [LinkedPayment] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTable()
        getData()
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @objc private func deleteDidTap(_ button: UIButton) {
        self.performSegue(withIdentifier: "goToDeleteBankAccountVC", sender: nil)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func getData() {
        ApiManager.getLinkedPayments(account: "abc") { (array) in
            self.arrayData = array
            self.tableView.reloadData()
        }
    }
}


//MARK:- TableView Extension
extension BanksVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == arrayData.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addBankCell")!
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PaymentDetailCell.cellID, for: indexPath) as! PaymentDetailCell
        cell.selectionStyle = .none
        cell.configCell(arrayData[indexPath.row])
        cell.deleteButton.addTarget(self, action: #selector(deleteDidTap(_:)), for: .touchUpInside)
        if openedCells.contains(indexPath) { cell.rotateIndicator() }
        if tappedCell == indexPath.row { cell.buttonAnimation(vc: self) }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == arrayData.count {
            return PaymentDetailCell.closedHeight
        }
        return openedCells.contains(indexPath) ? PaymentDetailCell.openHeight : PaymentDetailCell.closedHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == arrayData.count {
            return
        }
        tappedCell = indexPath.row
        if !openedCells.contains(indexPath) {
            openedCells.insert(indexPath)
        } else {
            openedCells.remove(indexPath)
        }
        tableView.reloadData()
    }
}
