//
//  TransactionsVC.swift
//  MavrikApp
//
//  Created by AK on 14.10.2020.
//

import UIKit


class TransactionsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var tableValues: [String: [AccountTransactions]] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        initTable()
        loadData()
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func loadData() {
        ApiManager.getAccountTransactions(account: "abc") { (array) in
            self.tableValues = self.parseObjects(array)
            self.tableView.reloadData()
            
        }
    }
}


//MARK:- TableView Extension
extension TransactionsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableValues.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let data = Array(tableValues.values)[section]
        return data.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionData = Array(tableValues.keys)
        let view = NotificationTableHeader().loadNib() as! NotificationTableHeader
        view.cellTitle.text = sectionData[section]
        view.cellTitle.textAlignment = .left
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = Array(tableValues.values)[indexPath.section][indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: TransactionCell.cellID, for: indexPath) as! TransactionCell
        cell.configCell(data)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TransactionCell.height
    }
    
}


//MARK:- Parse Date
extension TransactionsVC {
    private func parseObjects(_ array: [AccountTransactions]) -> [String: [AccountTransactions]] {
        var data: [String: [AccountTransactions]] = [:]
        for val in array {
            if let date = getDate(fromString: val.timestamp ?? "") {
                let month = getYear(from: date)
                if data[month] != nil {
                    var array = data[month]
                    array!.append(val)
                    data[month] = array!
                } else {
                    data[month] = [val]
                }
            }
        }
        return data
    }
    
    private func getDate(fromString str: String) -> Date? {
        let frmt = DateFormatter()
        frmt.dateFormat = "MMMM dd yyyy h:mm a"
        return frmt.date(from: str)
    }
    
    private func getYear(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: date)
    }
}
