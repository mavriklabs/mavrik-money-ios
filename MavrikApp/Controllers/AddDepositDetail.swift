//
//  AddDepositDetail.swift
//  MavrikApp
//
//  Created by AK on 19.10.2020.
//

import UIKit

class AddDepositDetail: UIViewController {
    
    @IBOutlet weak var vcTitle: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    
    var operationType: OperationType?
    var paymentDetail: LinkedPayment?
    var amount: String?
    
    private var doneDescr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        confirmButton.addTarget(self, action: #selector(confirmDidTap), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configData()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "dismissDeposit"), object: nil, queue: nil) { (_) in
       //     self.navigationController?.popToViewController(DepositVC(), animated: true)
            self.navigationController?.popToRootViewController(animated: true)
      //      self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func configUI() {
        if let type = operationType {
            switch type {
            case .deposit:
                vcTitle.text = "Deposit"
                typeLabel.text = "Deposit Details"
                doneDescr = "We have received your deposit request for \(amount ?? ""). The amount will be deposited shortly."
            case .withdraw:
                vcTitle.text = "Withdraw"
                typeLabel.text = "Withdraw Details"
                doneDescr = "We have received your withdrawal request."
            }
        }
    }
    
    private func loadUserData() {
        ApiManager.getUserData { (user) in
            if let user = user {
                self.nameLabel.text = user.name
            }
        }
    }
    
    private func configData() {
        amountLabel.text = amount
        totalLabel.text = amount
        bankLabel.text = paymentDetail?.name
        accountNumberLabel.text = paymentDetail?.id
        loadUserData()
    }
    
    @objc private func backDidTap() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func confirmDidTap() {
        self.sendData()
    }
    
    private func sendData() {
        ApiManager.addDeposit(account: "abc",
                                      param: ["amount": 100,
                                              "currency": "USD",
                                              "fees": 1,
                                              "source": "abc"], callback: { status in
                                                if status {
                                                    self.performSegue(withIdentifier: "successFromDeposit", sender: nil)
                                                }
                                              })
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "successFromDeposit" else { return }
        guard let destination = segue.destination as? SuccessfulVC else { return }
        SuccessfulVC.type = vcTitle.text!
        destination.model = SuccessfulModel(vcTitle: vcTitle.text!, descr: doneDescr ?? "We have received your deposit request for \(self.amount ?? "$0.00"). The amount will be deposited shortly.")
    }
}
