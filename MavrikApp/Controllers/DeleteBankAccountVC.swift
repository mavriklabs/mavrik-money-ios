//
//  DeleteBankAccountVC.swift
//  MavrikApp
//
//  Created by AK on 19.10.2020.
//

import UIKit

class DeleteBankAccountVC: UIViewController, KeyboardDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        fieldView.withRadius(10)
        textField.isSecureTextEntry = true
        textField.becomeFirstResponder()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        let keyboardView = NumpadView().loadNib() as! NumpadView
        keyboardView.delegate = self
        keyboardView.backgroundColor = .clear
        textField.inputView = keyboardView
        keyboardView.configView(vc: self, doneSelector: #selector(numDoneDidTap))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "dismissLinkedAcc"), object: nil, queue: nil) { (_) in
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @objc private func backDidTap() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func keyWasTapped(character: Int) {
        self.textField.font = UIFont.init(name: ("Montserrat-Bold"), size: 30.0)
        if character == 10 {
            if self.textField.text?.count != 0 {
                var text = self.textField.text!
                text.removeLast()
                self.textField.text = text
            }
            return
        }
        self.textField.text = self.textField.text! + String(character)
    }
    
    @objc func numDoneDidTap() {
        self.performSegue(withIdentifier: "successFromDeletingCard", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "successFromDeletingCard" else { return }
        guard let destination = segue.destination as? SuccessfulVC else { return }
        SuccessfulVC.type = "Linked Accounts"
        destination.model = SuccessfulModel(vcTitle: "Linked Accounts", descr: "Your Bank of America account was deleted from your linked accounts.")
    }
    
}

extension DeleteBankAccountVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
   //     textField.resignFirstResponder()
    //    showPinNumpad()
    }
}
