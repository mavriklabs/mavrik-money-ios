//
//  ViewController.swift
//  MavrikApp
//
//  Created by AK on 09.10.2020.
//

import UIKit
import Hero
import FirebaseAuth

class SplashVC: UIViewController {
    
    @IBOutlet weak var appLogo: UIImageView!
    private let localStorage = LocalStorage()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        startAnimation()
    }
    
    private func startAnimation() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            Animations.animateView(type: .popOut, view: self.appLogo, vc: self, completion: {})
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.7) {
            print("Getting screen to present from splash")
            let vcId = CustomUtils.getScreenToPresent()
            self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
        }
    }
}

