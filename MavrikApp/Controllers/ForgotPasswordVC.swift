//
//  ForgotPasswordVC.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit
import Hero
import FirebaseAuth
import Lottie

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var emailFieldView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fieldConstr: NSLayoutConstraint! // 40
    
    var loaderAnimation: LoaderAnimation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation(vc: self)
        emailField.becomeFirstResponder()
        emailField.delegate = self
        emailFieldView.withRadius(10)
        sendButton.withRadius(10)
        sendButton.addTarget(self, action: #selector(sendDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        initializeHideKeyboard()
        subscribeToNotification(UIResponder.keyboardWillShowNotification, selector: #selector(keyboardWillShow))
        subscribeToNotification(UIResponder.keyboardWillHideNotification, selector: #selector(keyboardWillHide))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromAllNotifications()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            animateConstraint(for: keyboardFrame.cgRectValue.height)
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        animateConstraint(for: 40)
    }
    
    @objc private func backDidTap() {
        dismissKeyboard()
        self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
    }
    
    private func animateConstraint(for change: CGFloat) {
        self.fieldConstr.constant = change
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func sendDidTap() {
        print("Send did tap")
        var errorMsg = ""
        let email = emailField.text ?? ""
        let emailValid = isValidEmail(email: email)
        if !emailValid {errorMsg += "Enter a valid email."}
        if !emailValid {
            emailFieldView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: emailFieldView, vc: self, completion: {})
            return
        }
        dismissKeyboard()
        emailFieldView.withBorder(color: .clear)
        let loaderView = LoaderView().loadNib() as! LoaderView
        self.loaderAnimation.showLoaderWithDone(withText: "Sending email", view: loaderView, done: false)
        Auth.auth().fetchSignInMethods(forEmail: email) { signInMethods, error in
            if (error != nil) {
               print("Firebase fetch sign in methods error: \(error!.localizedDescription)")
               self.handleError(error!)
               return
            }
            let signInMethod = signInMethods?[0]
            if signInMethod == nil {
                self.loaderAnimation.showMessage(withText: "No account found for this email.")
            }
            else if signInMethod == GoogleAuthSignInMethod {
                self.loaderAnimation.showMessage(withText: "This email is used to sign in via Google. Please use Google to reset your password.")
                return
            }
            else if signInMethod?.description == Constants.LoginMethods.appleProviderId {
                self.loaderAnimation.showMessage(withText: "This email is used to sign in via Apple. Please use Apple to reset your password.")
                return
            }
            else if signInMethod == EmailPasswordAuthSignInMethod {
                //send password reset email
                var urlComponents = URLComponents()
                urlComponents.scheme = "https"
                urlComponents.host = Constants.URL.base
                urlComponents.path = Constants.URL.emailActionsPath

                let actionCodeSettings =  ActionCodeSettings.init()
                actionCodeSettings.handleCodeInApp = true
                actionCodeSettings.url = urlComponents.url
                actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
                actionCodeSettings.dynamicLinkDomain = Constants.URL.fDLDomain
                
                Auth.auth().sendPasswordReset(withEmail: email, actionCodeSettings: actionCodeSettings) { error in
                    if (error != nil) {
                       print("Firebase send password reset email error: \(error!.localizedDescription)")
                       self.handleError(error!)
                       return
                    }
                    // show done
                    self.loaderAnimation.showLoaderWithDone(withText: "Email Sent", view: loaderView, done: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
                    }
                }
            }
        }
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
}


//MARK:- TextField Delegate
extension ForgotPasswordVC: UITextFieldDelegate {
    func subscribeToNotification(_ notification: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: notification, object: nil)
    }
    
    func unsubscribeFromAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendDidTap()
        return true
    }
    
    func initializeHideKeyboard() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
