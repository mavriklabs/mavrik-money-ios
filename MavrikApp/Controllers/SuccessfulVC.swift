//
//  SuccessfulVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit

class SuccessfulVC: UIViewController {

    @IBOutlet weak var vcTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var model: SuccessfulModel?
    
    static var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        showAnimation()
    }
    
    @objc private func backDidTap() {
        if self.navigationController != nil {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func showAnimation() {
        configVC()
        titleLabel.alpha = 0
        descrLabel.alpha = 0
        Animations.animateView(type: .fromLeft, duration: 1, view: titleLabel, vc: self, completion: {})
        Animations.animateView(type: .fromRight, duration: 1, view: descrLabel, vc: self, completion: {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.navigationController?.popToRootViewController(animated: true)
                SuccessfulVC.type = ""
            }
        })
    }
    
    private func configVC() {
        if let data = model {
            vcTitle.text = data.vcTitle
            descrLabel.text = data.descr
        }
    }
}
