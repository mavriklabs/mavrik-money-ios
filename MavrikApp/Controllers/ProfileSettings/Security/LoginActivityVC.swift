//
//  LoginActivityVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit

class LoginActivityVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var tableValues: [LoginActivity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    private func configTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK:- TableView Extension
extension LoginActivityVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LoginActivityCell.cellID, for: indexPath) as! LoginActivityCell
        cell.configCell(tableValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return LoginActivityCell.height
    }
}
