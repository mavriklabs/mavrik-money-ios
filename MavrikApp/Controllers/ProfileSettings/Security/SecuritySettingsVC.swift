//
//  SecuritySettingsVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit

class SecuritySettingsVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private var tableValues: [String] = [Constants.ProfileSettings.pin, Constants.ProfileSettings.biometric]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    private func configTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//MARK:- TableView Extension
extension SecuritySettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSettingsCell.cellID, for: indexPath) as! ProfileSettingsCell
        var title = tableValues[indexPath.row]
        if title == Constants.ProfileSettings.biometric {
            title = getBiometryType()
        }
        cell.cellTitle.text = title
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProfileSettingsCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cellTitle = tableValues[indexPath.row]
        switch cellTitle {
        case Constants.ProfileSettings.pin:
            self.performSegue(withIdentifier: "goToSetPinVC", sender: nil)
        case Constants.ProfileSettings.biometric:
            self.goToVC(withID: .FaceAndTouchIDVC, style: .formSheet)
        case Constants.ProfileSettings.loginActivity:
            self.goToVC(withID: .LoginActivityVC, style: .formSheet)
        default:
            print(" ") 
        }
    }
}
