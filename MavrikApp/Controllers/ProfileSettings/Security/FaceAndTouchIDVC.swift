//
//  FaceAndTouchIDVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit
import LocalAuthentication

class FaceAndTouchIDVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idIcon: UIImageView!
    @IBOutlet weak var idName: UILabel!
    
    @IBOutlet weak var enableIdLabel: UILabel!
    @IBOutlet weak var enableDescrLabel: UILabel!
    @IBOutlet weak var enableSwitch: UISwitch!
    
    @IBOutlet weak var enableVerificationLabel: UILabel!
    @IBOutlet weak var verificationDescrLabel: UILabel!
    @IBOutlet weak var verificationSwitch: UISwitch!
    
    private let storage = LocalStorage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configLabels()
        hideSectionAndVerificationLabels()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        enableSwitch.addTarget(self, action: #selector(switchDidChange(_:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSwitchStatus()
    }
    
    private func hideSectionAndVerificationLabels() {
        idName.isHidden = true
        enableVerificationLabel.isHidden = true
        verificationDescrLabel.isHidden = true
        verificationSwitch.isHidden = true
    }
    
    private func getSwitchStatus() {
        let touchType = storage.getAuthKey(key: .mfaTypeTouchId) ?? false
        let faceType = storage.getAuthKey(key: .mfaTypeFaceId) ?? false
        if touchType || faceType {
            enableSwitch.isOn = true
            verificationSwitch.isOn = true
        } else {
            enableSwitch.isOn = false
            verificationSwitch.isOn = false
        }
    }
    
    @objc private func switchDidChange(_ select: UISwitch) {
        storage.setAuthKey(key: .mfaTypeFaceId, value: select.isOn)
        // check if permissions are set
        if select.isOn {
            authenticateWithBiometrics() { status, errorCode in
                let response = status ?? false
                if !response {
                    self.storage.setAuthKey(key: .mfaTypeFaceId, value: false)
                    DispatchQueue.main.async {
                        self.enableSwitch.isOn = false
                    }
                    if errorCode == LAError.biometryNotAvailable.rawValue {
                        let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                        if let settingsUrl = settingsUrl, UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl)
                        }
                    } else {
                        print("Cannot turn on \(self.getBiometryType()).")
                    }
                }
            }
        }
    }
    
    private func configLabels() {
        switch getBiometryType() {
        case Constants.BiometryType.touch:
            titleLabel.text = "Touch ID"
            idIcon.image = UIImage(named: Constants.Images.touchIcon)
            idName.text = "Touch ID"
            enableIdLabel.text = "Enable Touch ID"
            enableDescrLabel.text = "Touch ID will be used as the verification method for payments, transactions and other services."
            enableVerificationLabel.text = "Use Touch ID for Verification"
            verificationDescrLabel.text = "Touch ID will be used as the verification method for payments, transactions and other services."
        case Constants.BiometryType.face:
            titleLabel.text = "Face ID"
            idIcon.image = UIImage(named: Constants.Images.faceIcon)
            idName.text = "Face ID"
            enableIdLabel.text = "Enable Face ID"
            enableDescrLabel.text = "Face ID will be used as the verification method for payments, transactions and other services."
            enableVerificationLabel.text = "Use Face ID for Verification"
            verificationDescrLabel.text = "Face ID will be used as the verification method for payments, transactions and other services."
        default:
            print("No biometry")
        }
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}
