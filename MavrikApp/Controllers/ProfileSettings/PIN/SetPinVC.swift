//
//  SetPinVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit
import FirebaseAuth
import FirebaseCore
import GoogleSignIn

class SetPinVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var currentPinView: UIView!
    @IBOutlet weak var currentPinField: UITextField!
    
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var pinField: UITextField!
    
    @IBOutlet weak var forgotPinButton: UIButton!
    @IBOutlet weak var confirmPinView: UIView!
    @IBOutlet weak var confirmPinField: UITextField!
    
    private var loaderAnimation: LoaderAnimation!
    private let storage = LocalStorage()
    private var socialLogin: SocialLogin!

    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation.init(vc: self)
        socialLogin = SocialLogin(vc: self, loaderAnimation: self.loaderAnimation, isSignUpFlow: false, isReauthFlow: true)
        currentPinField.delegate = self
        pinField.delegate = self
        confirmPinField.delegate = self
        currentPinField.isSecureTextEntry = true
        pinField.isSecureTextEntry = true
        confirmPinField.isSecureTextEntry = true
        configUI()
        addTargets()
        initHideKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(reAuthSuccessNotif),
                                name: NSNotification.Name.init(Constants.NotificationActions.reAuthSuccess), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configUI() {
        currentPinView.withRadius(10)
        pinView.withRadius(10)
        confirmPinView.withRadius(10)
        
        let pinSet = storage.getAuthKey(key: .mfaTypePin) ?? false
        if !pinSet {
            currentPinView.isHidden = true
            forgotPinButton.isHidden = true
            pinField.becomeFirstResponder()
        } else {
            currentPinField.becomeFirstResponder()
        }
    }
    
    private func addTargets() {
        confirmButton.addTarget(self, action: #selector(confirmDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        forgotPinButton.addTarget(self, action: #selector(forgotPinDidTap), for: .touchUpInside)
        currentPinField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        pinField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        confirmPinField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc private func textFieldDidChange(_ currentField: UITextField) {
        currentField.font = Constants.Fonts.montSerratBold25
        if currentField.text == "" {
            currentField.font = Constants.Fonts.montSerratRegular14
        }
    }
    
    @objc private func reAuthSuccessNotif() {
        // re-auth is successful
        currentPinView.isHidden = true
        forgotPinButton.isHidden = true
        
        // remove old pin
        print("Re-auth for reset PIN successful. Removing old PIN")
        storage.removeAuthKey(key: .pin)
        storage.removeAuthKey(key: .mfaTypePin)
        //loaderAnimation.showMessage(for: Constants.LoaderViews.setPinVCReAuthSuccess, withText: "Login successful. You can now set a new PIN.")
        let alert = UIAlertController(title: nil, message: "Login successful. You can now set a new PIN.",  preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { action in
            self.pinField.becomeFirstResponder()
        }
        alert.setMessage(font: Constants.Fonts.montSerratMedium14, color: nil)
        alert.addAction(ok)
        self.present(alert, animated: true)
    }
    
    
    private func checkCurrentPin() -> Bool {
        let pinSet = storage.getAuthKey(key: .mfaTypePin) ?? false
        if pinSet {
            let currentPin = currentPinField.text ?? ""
            if currentPin == "" {
                currentPinView.withBorder(color: Constants.Colors.defaultRed)
                Animations.animateView(type: .shake, view: currentPinView, vc: self, completion: {})
                return false
            }
            // clear border red
            dismissKeyboard()
            currentPinView.withBorder(color: .clear)
            
            let setPin = storage.getAuthKey(key: .pin) ?? ""
            if setPin != "" {
                // get hash of entered pin
                let currentPinHash = CustomUtils.getSHA256(for: currentPin)
                if currentPinHash != setPin {
                    loaderAnimation.showMessage(withText: "Current PIN is incorrect")
                    return false
                }
            }
        }
        
        return true
    }
    
    @objc private func confirmDidTap() {
        let isPinCorrect = checkCurrentPin()
        if !isPinCorrect {
            return
        }
        
        let pin = pinField.text ?? ""
        let confirmPin = confirmPinField.text ?? ""
        
        if pinField.text == "" {
            pinView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: pinView, vc: self, completion: {})
            return
        }
        if confirmPinField.text == "" {
            confirmPinView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: confirmPinView, vc: self, completion: {})
            return
        }
        // clear border red
        dismissKeyboard()
        pinView.withBorder(color: .clear)
        confirmPinView.withBorder(color: .clear)
        
        if pin.count < 4 {
            loaderAnimation.showMessage(withText: "New PIN must have minimum 4 digits")
            return
        }
        if pin != confirmPin {
            loaderAnimation.showMessage(withText: "New PINs don't match")
            return
        }
        
        // get hash of pin
        let hash = CustomUtils.getSHA256(for: pin)
        
        // store auth type and the actual pin in local storage
        self.storage.setAuthKey(key: .mfaTypePin, value: true)
        self.storage.setAuthKey(key: .pin, value: hash)
        storage.setProfileKey(key: .multiFactorSet, value: true)
        
        // update firebase
        let userId = Auth.auth().currentUser!.uid
        let data = [LocalProfileKeys.multiFactorSet.rawValue: true]
        CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
        
        self.performSegue(withIdentifier: "successFromSetPin", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "successFromSetPin" else { return }
        guard let destination = segue.destination as? SuccessfulVC else { return }
        SuccessfulVC.type = "PIN"
        destination.model = SuccessfulModel(vcTitle: "PIN", descr: "Your PIN was set successfully")
    }
    
    @objc private func backDidTap() {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func forgotPinDidTap() {
        //loaderAnimation.showActionPrompt(with: Constants.LoaderActions.reauth, text: "Forgot PIN?\n\nPlease re-login to reset it")
        let alert = UIAlertController(title: nil, message: "Please relogin to reset your PIN",  preferredStyle: .actionSheet)
        let deny = UIAlertAction(title: "Cancel", style: .default)
        let settings = UIAlertAction(title: "Login", style: .default) { action in
            self.reauth()
        }
        alert.setMessage(font: Constants.Fonts.montSerratMedium14, color: nil)
        alert.addAction(deny)
        alert.addAction(settings)
        self.present(alert, animated: true)
    }
    
    func reauth() {
        if let user = Auth.auth().currentUser {
            let providerId = storage.getProfileKey(key: .loginMethod) ?? ""
            print(providerId)
            switch providerId {
            case Constants.LoginMethods.emailPassword:
                reAuthWithEmail(user)
            case Constants.LoginMethods.google:
                signInWithGoogle()
            case Constants.LoginMethods.apple:
                socialLogin.startSignInWithAppleFlow()
            default:
                print("No supported login provider found")
            }
        }
    }
    
    private func reAuthWithEmail(_ user: User) {
        let email = user.email ?? ""
        if email == "" {
            loaderAnimation.showMessage(withText: "Email is empty. Are you logged in?")
            return
        }
        let passAlert = UIAlertController(title: "Enter your password to reset PIN", message: nil, preferredStyle: .alert)
        passAlert.setTitle(font: Constants.Fonts.montSerratMedium14, color: nil)
        passAlert.addTextField() { textField in
            textField.isSecureTextEntry = true
        }
        let submitAction = UIAlertAction(title: "Submit", style: .default) { _ in
            let password = passAlert.textFields![0].text ?? ""
            if password != "" {
                self.loaderAnimation.showLoader()
                let credential = EmailAuthProvider.credential(withEmail: email, password: password)
                let name = self.storage.getProfileKey(key: .name) ?? ""
                self.socialLogin.firebaseSignIn(name: name, withProvider: Constants.LoginMethods.emailPassword, credential: credential)
            }
        }
        passAlert.addAction(submitAction)
        present(passAlert, animated: true)
    }
    
    private func signInWithGoogle(){
        // Configure the Google Sign In instance
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()!.options.clientID
        GIDSignIn.sharedInstance().delegate = self.socialLogin
        // Start the sign in flow!
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
}

//MARK:- TextField Delegate Extension
extension SetPinVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func initHideKeyboard() {
        let tap = UITapGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
