//
//  ManageMembershipVC.swift
//  MavrikApp
//
//  Created by AK on 19.10.2020.
//

import UIKit

class ManageMembershipVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }

}
