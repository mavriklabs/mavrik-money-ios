//
//  MembershipSettings.swift
//  MavrikApp
//
//  Created by AK on 19.10.2020.
//

import UIKit


class MembershipSettingsVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    private var tableValues: [String] = ["Manage Membership", "Payment Details"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        addTargets()
    }
    
    private func addTargets() {
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    private func configTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK:- TableView Extension
extension MembershipSettingsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSettingsCell.cellID, for: indexPath) as! ProfileSettingsCell
        cell.cellTitle.text = tableValues[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProfileSettingsCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            self.goToVC(withID: .ManageMembershipVC, style: .formSheet)
        case 1:
            self.goToVC(withID: .BanksVC, style: .formSheet)
        default:
            print(" ")
        }
    }
}
