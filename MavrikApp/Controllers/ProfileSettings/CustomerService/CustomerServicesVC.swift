//
//  CustomerServicesVC.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit

class CustomerServicesVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!

    private var tableValues: [String] = ["Email us"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    }
    
    private func configTable() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
}


//MARK:- TableView Extension
extension CustomerServicesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileSettingsCell.cellID, for: indexPath) as! ProfileSettingsCell
        let labelText = tableValues[indexPath.row]
        if labelText == "Email us" {
            let email = Constants.CustomerService.email
            let wholeStr = NSMutableAttributedString.init(string: "Email us : \(email)")
            let rangeToColor = NSRange(location: 11, length: email.count)
            wholeStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Constants.Colors.defaultOrange, range: rangeToColor)
            cell.cellTitle.attributedText = wholeStr
        } else {
            cell.cellTitle.text = labelText
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ProfileSettingsCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let labelText = tableValues[indexPath.row]
        if labelText == "Email us" {
            sendEmailToSupport()
        }
    }
}
