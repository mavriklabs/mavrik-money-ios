//
//  ChatVC.swift
//  MavrikApp
//
//  Created by AK on 16.10.2020.
//

import UIKit

class ChatVC: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageViewConstr: NSLayoutConstraint!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    private var keyboardHeight: CGFloat = 0
    private var tableValues: [(String, Bool)] = [("", false)]
    
    private var showCellAnimation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTable()
        initUI()
        textField.delegate = self
        observeToKeyboard()
        sendButton.addTarget(self, action: #selector(sendDidTap), for: .touchUpInside)
        textField.addTarget(self, action: #selector(fieldDidChange(_:)), for: .editingChanged)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        messageViewConstr.constant = 0
    }
    
    private func initTable() {
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
    }
    
    private func initUI() {
        textFieldView.withRadius(15)
    }
    
    private func observeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        if keyboardHeight == 0 {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                keyboardHeight = keyboardSize.height
            }
        }
        messageViewConstr.constant = -300
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        messageViewConstr.constant = 0
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func sendDidTap() {
        if textField.text == nil { return }
        showCellAnimation = true
        tableValues.append((textField.text!, true))
        textField.text = nil
        sendButton.setImage(UIImage(named: "send_grey"), for: .normal)
        self.tableView.reloadData()
        scrollTableToBottom()
    }
    
    @objc private func fieldDidChange(_ field: UITextField) {
        sendButton.setImage(UIImage(named: (field.text == nil) ? "send_grey" : "send"), for: .normal)
    }
    
    private func scrollTableToBottom() {
        DispatchQueue.main.async {
            if self.tableValues.count > 0 {
            let indexPath = IndexPath(row: self.tableValues.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    private func botMenuDidTap(withText text: String) {
        tableValues.append((text, true))
        showCellAnimation = true
        self.tableView.reloadData()
        scrollTableToBottom()
    }
}


//MARK:- TableView Extension
extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableValues[indexPath.row].1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: YourMessageCell.cellID, for: indexPath) as! YourMessageCell
            cell.selectionStyle = .none
            cell.configCell(withText: tableValues[indexPath.row].0)
            
            if showCellAnimation && indexPath.row == tableValues.count - 1 {
                cell.messageView.alpha = 0
                self.scrollTableToBottom()
                Animations.animateView(type: .popIn, view: cell.messageView, vc: self, completion: {})
                showCellAnimation = false
            }
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: BotMessageCell.cellID, for: indexPath) as! BotMessageCell
        cell.selectionStyle = .none
        cell.buttonDidTap = { (buttonNum) in
            switch buttonNum {
            case 1:
                self.botMenuDidTap(withText: "Lorem ipsum dolor sit amet.")
            case 2:
                self.botMenuDidTap(withText: "Lorem ipsum doler.")
            case 3:
                self.botMenuDidTap(withText: "Doler sit amet.")
            default:
                print("")
            }
            
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableValues[indexPath.row].1 {
            return UITableView.automaticDimension
        }
        return BotMessageCell.height
    }
}


//MARK:- TextField Delegate Extension
extension ChatVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.returnKeyType = .done
    }
}

