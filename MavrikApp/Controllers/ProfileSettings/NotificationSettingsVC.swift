//
//  NotificationSettingsVC.swift
//  MavrikApp
//
//  Created by AK on 15.10.2020.
//

import UIKit
import UserNotifications
import FirebaseAuth

class NotificationSettingsVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var notificationSwitcher: UISwitch!
    
    private let storage = LocalStorage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        notificationSwitcher.addTarget(self, action: #selector(notifDidSwitch(_:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let pref = storage.getProfileKey(key: .pushNotifications) ?? false
        NotificationManager.registerForAPNS() { status in
            // if pref is on but system settings are off, set pref to off
            DispatchQueue.main.async {
                if pref && !status {
                    self.notificationSwitcher.isOn = false
                } else {
                    self.notificationSwitcher.isOn = pref
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let status = storage.getProfileKey(key: .pushNotifications) ?? false
        NotificationManager.updateNotifPrefs(status)
        super.viewWillDisappear(animated)
    }
    
    @objc private func backDidTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func notifDidSwitch(_ switcher: UISwitch) {
        self.storage.setProfileKey(key: .pushNotifications, value: switcher.isOn)
        if switcher.isOn {
            NotificationManager.registerForAPNS() { status in
                if !status {
                    //open settings app
                    DispatchQueue.main.async {
                        let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                        if let settingsUrl = settingsUrl, UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl)
                        }
                    }
                }
            }
        }
    }
}

struct NotificationManager {
    
    private static let center = UNUserNotificationCenter.current()
    private static let storage = LocalStorage()
    
    private static func requestAPNSAuthz() {
        center.requestAuthorization(options: [.alert, .sound, .badge, .providesAppNotificationSettings]) {
            (granted, error) in
            print("Push notifications permission: \(granted)")
            if granted {
                registerForRemoteNotifications()
            }
            updateNotifPrefs(granted)
        }
    }
    
    private static func registerForRemoteNotifications() {
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    static func registerForAPNS(callback: @escaping(Bool) -> Void) {
        center.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .authorized {
                registerForRemoteNotifications()
                callback(true)
            } else if settings.authorizationStatus == .denied {
                callback(false)
            } else {
                requestAPNSAuthz()
            }
        })
    }
    
    static func updateNotifPrefs(_ status: Bool) {
        self.storage.setProfileKey(key: .pushNotifications, value: status)
        // store push notification preferences in firebase profile
        let userId = Auth.auth().currentUser!.uid
        let data = [LocalProfileKeys.pushNotifications.rawValue: status]
        CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId, data: data)
    }
}
