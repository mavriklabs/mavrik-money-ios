//
//  VerifyEmailVC.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit
import Hero
import FirebaseAuth

class VerifyEmailVC: UIViewController {
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    var loaderAnimation: LoaderAnimation!
    let storage = LocalStorage()
    private var shouldVerifyEmail = false
    private var verifyEmailCode = ""
    private var backButtonHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation(vc: self)
        resendButton.addTarget(self, action: #selector(resendButtonDidTap), for: .touchUpInside)
        if backButtonHidden {
            backButton.isHidden = true
        } else {
            backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        }
        doneButton.addTarget(self, action: #selector(doneDidTap), for: .touchUpInside)
        
        // send verification email
        let user = Auth.auth().currentUser
        if let user = user {
            if !user.isEmailVerified {
                CustomUtils.sendVerificationEmail() { _ in
                    let email = Auth.auth().currentUser?.email ?? ""
                    print("Verification email sent to: " + email)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // check if there is a logged in user
        let user = Auth.auth().currentUser ?? nil
        if user == nil {
            print("User is nil, redirecting to sign in")
            self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
            return
        }
        if shouldVerifyEmail {
            verifyEmail(verifyEmailCode)
        }
    }
    
    @objc private func backDidTap() {
        self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
    }
    
    @objc private func doneDidTap() {
        self.loaderAnimation.showLoader(withText: "Verifying")
        reloadAndVerify()
    }
    
    @objc private func resendButtonDidTap() {
        self.loaderAnimation.showLoader(withText: "Sending new link")
        CustomUtils.sendVerificationEmail() { _ in
            self.loaderAnimation.removeLoader(withAnimation: .popOut)
            let email = Auth.auth().currentUser?.email ?? ""
            print("Resent verification email to: " + email)
        }
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
    
    func hideBackButton() {
        backButtonHidden = true
    }
    
    func setShouldVerifyEmail(_ shouldVerify: Bool) {
        shouldVerifyEmail = shouldVerify
    }
    
    func setVerifyEmailCode(_ code: String) {
        verifyEmailCode = code
    }
    
    func verifyEmail(_ code: String) {
        self.loaderAnimation.showLoader(withText: "Verifying email")
        print("Verifying email with code: " + verifyEmailCode)
        let verified = Auth.auth().currentUser?.isEmailVerified ?? false
        if verified {
            print("Email already verified")
            let vcId = CustomUtils.getScreenToPresent()
            self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
            return
        }
        Auth.auth().applyActionCode(code) { error in
            if error != nil {
                print("Verify email failed with error: \(error!.localizedDescription)")
                self.handleError(error!)
                return
            }
            self.reloadAndVerify()
        }
    }
    
    private func reloadAndVerify() {
        Auth.auth().currentUser?.reload() { error in
            if error != nil {
                print("User reload failed with error: \(error!.localizedDescription)")
                self.handleError(error!)
                return
            }
            let verified = Auth.auth().currentUser?.isEmailVerified ?? false
            if verified {
                let data = ["emailVerified": true]
                // update email verified in firebase
                let userId = Auth.auth().currentUser?.uid
                CustomUtils.updateFirestoreDoc(collection: Constants.Firestore.usersCollection, docId: userId!, data: data)
                
                let vcId = CustomUtils.getScreenToPresent()
                self.goToVC(withID: vcId, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right)))
                return
            } else {
                self.loaderAnimation.showMessage(withText: "Email could not be verified.")
            }
        }
    }
}
