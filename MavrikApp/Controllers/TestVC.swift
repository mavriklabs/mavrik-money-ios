//
//  TestVC.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/21/20.
//

import Foundation
import Lottie

class TestVC: UIViewController {
    
    var loaderAnimation: LoaderAnimation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let animationView = AnimationView.init(name: "checkMark")
        //        animationView.frame = view.bounds
        //        // 3. Set animation content mode
        //        animationView.contentMode = .scaleAspectFit
        //        // 4. Set animation loop mode
        //        animationView.loopMode = .loop
        //        // 5. Adjust animation speed
        //        animationView.animationSpeed = 1
        //        view.addSubview(animationView)
        //        // 6. Play animation
        //        animationView.play()
        
        loaderAnimation = LoaderAnimation(vc: self)
        let loaderView = LoaderView().loadNib() as! LoaderView
        loaderAnimation.showLoaderWithDone(withText: "Sending email", view: loaderView, done: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.loaderAnimation.showLoaderWithDone(withText: "Email Sent", view: loaderView, done: true)
        }
    }
}
