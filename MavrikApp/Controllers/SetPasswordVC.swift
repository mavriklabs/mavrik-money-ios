//
//  SetPasswordVC.swift
//  MavrikApp
//
//  Created by AK on 26.10.2020.
//

import UIKit
import Hero
import FirebaseAuth

class SetPasswordVC: UIViewController {
    
    @IBOutlet weak var passFieldView: UIView!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fieldConstr: NSLayoutConstraint! // 40
    
    private var keyboardHeight: CGFloat = 0
    var loaderAnimation: LoaderAnimation!
    private var passResetCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loaderAnimation = LoaderAnimation(vc: self)
        passField.becomeFirstResponder()
        passField.delegate = self
        passFieldView.withRadius(10)
        setButton.withRadius(10)
        setButton.addTarget(self, action: #selector(setDidTap), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
        initializeHideKeyboard()
        subscribeToNotification(UIResponder.keyboardWillShowNotification, selector: #selector(keyboardWillShow))
        subscribeToNotification(UIResponder.keyboardWillHideNotification, selector: #selector(keyboardWillHide))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromAllNotifications()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            animateConstraint(for: keyboardFrame.cgRectValue.height)
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) {
        animateConstraint(for: 40)
    }
    
    private func animateConstraint(for change: CGFloat) {
        self.fieldConstr.constant = change
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func backDidTap() {
        dismissKeyboard()
        self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
    }
    
    @objc private func setDidTap() {
        let pass = passField.text ?? ""
        if !isValidPassword(pass: pass) {
            passFieldView.withBorder(color: Constants.Colors.defaultRed)
            Animations.animateView(type: .shake, view: passFieldView, vc: self, completion: {})
            return
        }
        dismissKeyboard()
        passFieldView.withBorder(color: .clear)
        let loaderView = LoaderView().loadNib() as! LoaderView
        self.loaderAnimation.showLoaderWithDone(withText: "Setting new password", view: loaderView, done: false)
        //verify password reset code
        Auth.auth().verifyPasswordResetCode(passResetCode) { email, error in
            if error != nil {
                print("Error occured while verifying password reset code: \(error!.localizedDescription)")
                self.handleError(error!)
                return
            }
            //set new password
            Auth.auth().confirmPasswordReset(withCode: self.passResetCode, newPassword: pass) { error in
                if error != nil {
                    print("Error occured while setting new password: \(error!.localizedDescription)")
                    self.handleError(error!)
                    return
                }
                // show done
                self.loaderAnimation.showLoaderWithDone(withText: "Password reset successful", view: loaderView, done: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.goToVC(withID: .SignInVC, style: .fullScreen, transition: HeroDefaultAnimationType.selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left)))
                }
            }
        }
    }
    
    func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            print(errorCode.errorMessage)
            self.loaderAnimation.showMessage(withText: errorCode.errorMessage)
        }
    }
    
    func setPassResetCode(_ code: String) {
        passResetCode = code
    }
    
}

//MARK:- TextField Delegate
extension SetPasswordVC: UITextFieldDelegate {
    func subscribeToNotification(_ notification: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: notification, object: nil)
    }
    
    func unsubscribeFromAllNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        setDidTap()
        return true
    }
    
    func initializeHideKeyboard() {
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        swipe.direction = .down
        swipe.cancelsTouchesInView = false
        self.view.addGestureRecognizer(swipe)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
