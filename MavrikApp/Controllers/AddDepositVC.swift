//
//  AddDepositVC.swift
//  MavrikApp
//
//  Created by AK on 19.10.2020.
//

import UIKit

enum OperationType {
    case deposit, withdraw
}

class AddDepositVC: UIViewController {

    @IBOutlet weak var vcTitle: UILabel!
    @IBOutlet weak var vcSubtitle: UILabel!
    @IBOutlet weak var vcDescr: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fieldView: UIView!
    @IBOutlet weak var textField: UITextField!
    
    var operationType: OperationType?
    var paymentDetail: LinkedPayment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        fieldView.withRadius(10)
        backButton.addTarget(self, action: #selector(backDidTap), for: .touchUpInside)
    //    textField.addTarget(self, action: #selector(fieldDidChange(_:)), for: .editingChanged)
        configUI()
        textField.becomeFirstResponder()
        
        let keyboardView = NumpadView().loadNib() as! NumpadView
                keyboardView.delegate = self
        keyboardView.backgroundColor = .clear
        textField.inputView = keyboardView
        keyboardView.configView(vc: self, doneSelector: #selector(numDoneDidTap))
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    private func configUI() {
        if let type = operationType {
            switch type {
            case .deposit:
                vcSubtitle.isHidden = true
                vcTitle.text = "Deposit"
                vcDescr.text = "How much do you want to deposit? (USD)"
            case .withdraw:
                vcSubtitle.isHidden = false
                vcTitle.text = "Withdraw"
                vcDescr.text = "How much do you want to withdraw? (USD)"
            }
        }
    }
    
    @objc private func backDidTap() {
        if self.navigationController != nil {
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func numDoneDidTap() {
        self.performSegue(withIdentifier: "goToAddDepositDetail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "goToAddDepositDetail" else { return }
        guard let destination = segue.destination as? AddDepositDetail else { return }
        destination.paymentDetail = self.paymentDetail
        destination.amount = textField.text
        destination.operationType = operationType
    }
        
}

//MARK:- Keyboard Delegate Extension
extension AddDepositVC: KeyboardDelegate {
    func keyWasTapped(character: Int) {
        if character == 10 {
            if self.textField.text?.count != 0 {
                var text = self.textField.text!
                text.removeLast()
                self.textField.text = text
                if text.count == 1 {
                    textField.text = "$0"
                }
            } else {
                textField.text = "$0"
            }
            return
        }
        if textField.text == "$0" {
            textField.text = "$"
            let replacedText = PhoneMaskManager.format(with: "$XXXXXXXXXXX", phone: self.textField.text! + String(character))
            textField.text = replacedText
            return
        }
        let replacedText = PhoneMaskManager.format(with: "$XXXXXXXXXXX", phone: self.textField.text! + String(character))
        textField.text = replacedText
    }
}


extension AddDepositVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
  //      textField.resignFirstResponder()
    //    showPinNumpad()
    }
}
