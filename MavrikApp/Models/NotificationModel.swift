//
//  NotificationModel.swift
//  MavrikApp
//
//  Created by AK on 17.11.2020.
//

import Foundation

struct NotificationModel: Codable {
    var id: String?
    var title: String?
    var desc: String?
    var read: Bool?
    var status: String?
    var timestamp: String?
    var action: String?
}
