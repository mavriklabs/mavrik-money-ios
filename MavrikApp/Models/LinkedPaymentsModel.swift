//
//  LinkedPaymentsModel.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import Foundation


struct LinkedPaymentsModel: Decodable {
    var linkedPayments: [LinkedPayment]?
}

struct LinkedPayment: Decodable {
    var name: String?
    var type: String?
    var id: String?
}
