//
//  ProfileModel.swift
//  MavrikApp
//
//  Created by AK on 17.11.2020.
//

import Foundation


struct ProfileModel: Decodable {
    var name: String?
    var email: String?
    var phone: String?
    var address: AddressModel?
}

struct AddressModel: Decodable {
    var street1: String?
    var city: String?
    var state: String?
    var postalCode: Int
}
