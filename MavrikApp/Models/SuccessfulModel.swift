//
//  SuccessfulModel.swift
//  MavrikApp
//
//  Created by AK on 23.11.2020.
//

import Foundation

struct SuccessfulModel {
    let vcTitle: String
    let descr: String
}
