//
//  HomeAccountModel.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import Foundation


struct HomeAccountModel: Decodable {
    var balance: Int?
    var currency: String?
    var yield: String?
    var earned: Int?
}
