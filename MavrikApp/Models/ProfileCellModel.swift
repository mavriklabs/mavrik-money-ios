//
//  ProfileCellModel.swift
//  MavrikApp
//
//  Created by AK on 13.10.2020.
//

import Foundation

struct ProfileCellModel {
    let iconName: String
    let title: String
    let descr: String?
}
