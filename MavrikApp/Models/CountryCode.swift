//
//  CountryCode.swift
//  MavrikApp
//
//  Created by AK on 21.11.2020.
//

import Foundation


struct CountryCode: Codable {
    let name: String
    let dial_code: String
    let code: String
}
