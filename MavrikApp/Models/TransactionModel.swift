//
//  TransactionModel.swift
//  MavrikApp
//
//  Created by AK on 23.11.2020.
//

import Foundation


enum TransactionType {
    case plus, minus
}

struct TransactionModel {
    let title: String
    let cardDetal: String
    let date: Date
    let price: String
    let transactionType: TransactionType
}
