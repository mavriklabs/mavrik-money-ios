//
//  KYCDocs.swift
//  Mavrik
//
//  Created by Adi Kancherla on 12/14/20.
//

import Foundation

struct KYCDocs: Decodable {
    let docs: [String]
}
