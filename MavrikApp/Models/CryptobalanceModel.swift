//
//  CryptoAccountModel.swift
//  MavrikApp
//
//  Created by AK on 15.11.2020.
//

import Foundation

struct CryptobalanceAccounts: Decodable {
    var numAccounts: Int?
    var accounts: [CryptobalanceModel]
}



struct CryptobalanceModel: Decodable {
    var name: String?
    var balance: Double?
    var currency: String?
    var id: String?
    var yield: String?
    var priceUSD: Int?
}
