//
//  LoginActivity.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import Foundation


struct LoginActivity: Decodable {
    var place: String?
    var lastActive: String?
    var device: String?
}
