//
//  AccountTransactionsModel.swift
//  MavrikApp
//
//  Created by AK on 19.11.2020.
//

import Foundation


struct AccountTransactionsModel: Decodable {
    var txns: [AccountTransactions]?
}


struct AccountTransactions: Decodable {
    var desc: String?
    var type: String?
    var currency: String?
    var amount: String?
    var source: String?
    var dest: String?
    var timestamp: String?
}
