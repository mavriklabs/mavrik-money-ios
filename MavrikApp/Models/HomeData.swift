//
//  HomeData.swift
//  MavrikApp
//
//  Created by AK on 15.11.2020.
//

import Foundation


struct HomeData: Decodable {
    var numAccounts: Int?
    var accounts: [AccountData]
}

struct AccountData: Decodable {
    var name: String?
    var balance: Double?
    var currency: String?
    var id: String?
    var yield: String?
}
